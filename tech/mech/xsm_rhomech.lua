function init()
  self.active = false
  tech.setVisible(false)
  tech.rotateGroup("guns", 0, true)
  self.holdingJump = false
  self.holdingUp = false
  self.holdingDown = false
  self.holdingLeft = false
  self.holdingRight = false
  self.holdingFire = false
  self.firePressed = false
  self.fireReleased = false
  self.holdingAltFire = false
  self.altFirePressed = false
  self.altFireReleased = false
  self.mechFlipped = false
  self.forceWalk = false
  
  -- Activate/Deactivate
  self.mechState = "off" -- Possible values "off" "turningOn" "on" "turningOff"
  self.mechStateTimer = 0
  
  -- Init landing variables
  self.fastDrop = false
  self.veryFastDrop = false
  self.veryVeryFastDrop = false
  self.movementSuppressed = false
  self.immobileTimer = 0
  
  -- Init jumping variables (short jump, dash, boost)
  self.isJumping = false
  self.hasJumped = false
  self.jumpPressed = false
  self.jumpReleased = false
  self.jumpTimer = 0
  self.jumpCoolDown = 0
  self.isDashing = false
  self.dashTimer = 0
  self.dashDirection = 0
  self.dashLastInput = 0
  self.dashTapLast = 0
  self.dashTapTimer = 0
  self.isBoosting = false
  self.boostTimer = 0
  self.backHopTimer = 0
  self.isCrouching = false
  
  -- Weapon set
  self.weaponEquipped = 0
  
  -- Init melee variables
  self.rightArmOccupied = false
  self.leftArmOccupied = false
  self.frontArmOccupied = false
  self.backArmOccupied = false
  self.meleeTimer = 0
  self.oldMeleeTimer = 0
  self.twohswing = false
  self.weaponState = "idle"
  self.attackQueued = false
  self.attacking = false
  self.projectilesSpawned = 0
  self.forceWalkMelee = false
  
  -- Init tractor beam
  self.entityQueryTable = {}
  self.itemExceptionTable = {}
  self.altWeaponState = "idle"
  self.altTimer = 0
  self.forceWalkBeam = false
  self.beamTimer = 0
  
  -- Init machine gun
  self.mGunTimer = 0
  self.mGunState = "idle"
  self.forceWalkGun = false
  self.mGunRecoilKick = 0
  
end

function uninit()
  if self.active then
    local mechTransformPositionChange = tech.parameter("mechTransformPositionChange")
    mcontroller.translate({-mechTransformPositionChange[1], -mechTransformPositionChange[2]})
    tech.setParentOffset({0, 0})
    self.active = false
    tech.setVisible(false)
    tech.setParentState()
    tech.setToolUsageSuppressed(false)
    mcontroller.controlFace(nil)

	-- Animation / Particles
	tech.setAnimationState("jetFlame", "off")
	tech.setAnimationState("movement", "idle")
	tech.setAnimationState("frontWeapon1", "off")
	tech.setAnimationState("backWeapon1", "off")
	tech.setAnimationState("frontWeapon2", "off")
	tech.setAnimationState("backWeapon2", "off")
	tech.setAnimationState("frontFiring", "off")
	tech.setAnimationState("backFiring", "off")
	tech.setAnimationState("frontArmMovement", "off")
	tech.setAnimationState("backArmMovement", "off")
	tech.setAnimationState("frontArmSwordMovement", "off")
	tech.setAnimationState("backArmSwordMovement", "off")
	tech.setAnimationState("frontArmGunMovement", "off")
	tech.setAnimationState("backArmGunMovement", "off")
	tech.setAnimationState("movement", "idle")
	tech.setAnimationState("torso", "idle")
	tech.setParticleEmitterActive("boostParticles", false)
	tech.setParticleEmitterActive("dashParticles", false)
	tech.setParticleEmitterActive("skidParticles", false)
	tech.setParticleEmitterActive("veryHardLandingParticles", false)
	tech.setParticleEmitterActive("hardLandingParticles", false)
	tech.setParticleEmitterActive("mechSteam2", false)
	tech.setParticleEmitterActive("mechSteam", false)
	
  end
end

function input(args)
  -- Check if player is holding jump first
  if args.moves["jump"] then
    self.holdingJump = true
  elseif not args.moves["jump"] then
    self.holdingJump = false
  end
  
  -- Checks if jump was released
  if not self.jumpReleased then
    if self.holdingJump and not self.jumpPressed and not self.jumpReleased then
	  self.jumpPressed = true
    elseif not self.holdingJump and self.jumpPressed and not self.jumpReleased then
	  self.jumpReleased = true
	  self.jumpPressed = false
    end
  end
  
  -- Check if player is holding fire
  if args.moves["primaryFire"] then
    self.holdingFire = true
  elseif not args.moves["primaryFire"] then
    self.holdingFire = false
  end 
 
  -- Checks if fire was released
  if not self.fireReleased then
    if args.moves["primaryFire"] and not self.firePressed and not self.fireReleased then
	  self.firePressed = true
    elseif not args.moves["primaryFire"] and self.firePressed and not self.fireReleased then
	  self.fireReleased = true
	  self.firePressed = false
    end
  end
  
  -- Check if player is holding altFire
  if args.moves["altFire"] then
    self.holdingAltFire = true
  elseif not args.moves["altFire"] then
    self.holdingAltFire = false
  end 
 
  -- Checks if altFire was released
  if not self.altFireReleased then
    if args.moves["altFire"] and not self.altFirePressed and not self.altFireReleased then
	  self.altFirePressed = true
    elseif not args.moves["altFire"] and self.altFirePressed and not self.altFireReleased then
	  self.altFireReleased = true
	  self.altFirePressed = false
    end
  end
  
  -- Checks if player is holding other buttons
  if args.moves["up"] then
    self.holdingUp = true
  elseif not args.moves["up"] then
    self.holdingUp = false
  end
  
  if args.moves["down"] then
    self.holdingDown = true
  elseif not args.moves["down"] then
    self.holdingDown = false
  end
  
  if args.moves["left"] then
    self.holdingLeft = true
  elseif not args.moves["left"] then
    self.holdingLeft = false
  end
  
  if args.moves["right"] then
    self.holdingRight = true
  elseif not args.moves["right"] then
    self.holdingRight = false
  end

  -- Double tap input for boosting -- blatantly stolen from dash.lua. Thanks Chucklefish!
  if self.dashTimer <= 0 then
	  local maximumDoubleTapTime = tech.parameter("maximumDoubleTapTime")
	  local dashDuration = tech.parameter("dashDuration")

	  if self.dashTapTimer > 0 then
		self.dashTapTimer = self.dashTapTimer - args.dt
	  end

	  if args.moves["right"] then
		if self.dashLastInput ~= 1 then
		  if self.dashTapLast == 1 and self.dashTapTimer > 0 then
			self.dashTapLast = 0
			self.dashTapTimer = 0
			return "dashRight"
		  else
			self.dashTapLast = 1
			self.dashTapTimer = maximumDoubleTapTime
		  end
		end
		self.dashLastInput = 1
	  elseif args.moves["left"] then
		if self.dashLastInput ~= -1 then
		  if self.dashTapLast == -1 and self.dashTapTimer > 0 then
			self.dashTapLast = 0
			self.dashTapTimer = 0
			return "dashLeft"
		  else
			self.dashTapLast = -1
			self.dashTapTimer = maximumDoubleTapTime
		  end
		end
		self.dashLastInput = -1
	  else
		self.dashLastInput = 0
	  end
  end  
  
  if args.moves["special"] == 1 then
    if self.active then
      return "mechDeactivate"
    else
      return "mechActivate"
    end
  elseif args.moves["primaryFire"] then
    return "mechFire"
  end

  return nil
end

function mechSetArmOccupied(side, state)
  if side == "right" then
    self.rightArmOccupied = state
  elseif side == "left" then
    self.leftArmOccupied = state
  end
end

function mechCheckArmOccupied(side)
  if side == "right" then
    return self.rightArmOccupied
  elseif side == "left" then
    return self.leftArmOccupied
  elseif side == "front" then
    return self.frontArmOccupied
  elseif side == "back" then
    return self.backArmOccupied
  end
end

function mechAnimateActiveArm(frontState, backState, anim, side, flipState)
-- Animates a given arm (side = "left" or "right"). flipState is a global that keeps track of mech direction.
  if anim == nil then
    return 0
  end

  if side == "right" then
    if flipState then
	  if backState ~= nil then tech.setAnimationState(backState, anim) end
	else
	  if frontState ~= nil then tech.setAnimationState(frontState, anim) end
	end
  elseif side == "left" then
    if flipState then
	  if frontState ~= nil then tech.setAnimationState(frontState, anim) end
	else
	  if backState ~= nil then tech.setAnimationState(backState, anim) end
	end
  else
    -- Error message
  end
end

function mechAnimateArms(frontState, backState, frontAltState, backAltState, altSide, flipState, anim)
-- Animates front and back arms, if they are not busy doing something else. Used for walking animations etc.
-- Tracks asymmetry. Alt indicates the asymmetrical side.

  if not mechCheckArmOccupied(altSide) then
    mechAnimateActiveArm(frontAltState, backAltState, anim, altSide, flipState)
    mechAnimateActiveArm(frontState, backState, "off", altSide, flipState)
  end
  if not mechCheckArmOccupied(otherSide(altSide)) then
    mechAnimateActiveArm(frontState, backState, anim, otherSide(altSide), flipState)
    mechAnimateActiveArm(frontAltState, backAltState, "off", otherSide(altSide), flipState)
  end
end

function mechAnimatePauldron(animState, anim)
  if not mechCheckArmOccupied("front") then
    tech.setAnimationState(animState, anim)
  end
end

function mechChooseObject(frontObject, backObject, side, flipState)
  if side == "right" then
    if flipState then
	  return backObject
	else
	  return frontObject
	end
  elseif side == "left" then
    if flipState then
	  return frontObject
	else
	  return backObject
	end
  elseif side == nil then
    if flipState then
	  return backObject
	else
	  return frontObject
	end
  end
end

function releaseInput(side)
  if side == "right" then
    self.altFireReleased = false
  elseif side == "left" then
    self.fireReleased = false
  end
end

-- Chooses an animation from animList based on the given angle between angleMin and angleMax
function chooseAnimAngle(angle, angleMin, angleMax, animList, flip)
  if #animList == 0 then
    return nil
  end
  local angleStep = (angleMax - angleMin) / #animList
  if flip then
    if angle > 0 then
      angle = math.pi - angle
	else
	  angle = -math.pi - angle
	end
  end
  if angle > angleMax then
    return animList[#animList]
  elseif angle <= angleMin then
    return animList[1]
  end
  for i, v in ipairs(animList) do
	if angle > (i-1) * angleStep + angleMin and angle <= i * angleStep + angleMin then
	  return animList[i]
	end
  end
  return nil
end

function update(args)
  -- Energy usage
  local energyCostPerSecond = tech.parameter("energyCostPerSecond")
  local energyCostPerSecondRunning = tech.parameter("energyCostPerSecondRunning")
  local energyCostPerDash = tech.parameter("energyCostPerDash")
  local energyCostPerBackHop = tech.parameter("energyCostPerBackHop")
  local energyCostPerSecondBoost = tech.parameter("energyCostPerSecondBoost")
  local energyCostPerBoost = tech.parameter("energyCostPerBoost")
  local energyCostPerSlash = tech.parameter("energyCostPerSlash")
  local energyCostPerSecondPull = tech.parameter("energyCostPerSecondPull")
  local energyCostPerBullet = tech.parameter("energyCostPerBullet")
  local energyCostAccumulated = 0
  
  -- Movement / misc
  local mechCustomMovementParameters = tech.parameter("mechCustomMovementParameters")
  local mechCustomTurningOnOffMovementParameters = copy(tech.parameter("mechCustomMovementParameters"))
  mechCustomTurningOnOffMovementParameters.runSpeed = 0.0
  mechCustomTurningOnOffMovementParameters.walkSpeed = 0.0
  local mechTransformPositionChange = tech.parameter("mechTransformPositionChange")
  local parentOffset = tech.parameter("parentOffset")
  local mechCollisionTest = tech.parameter("mechCollisionTest")
  local mechBackwardSpeed = tech.parameter("mechBackwardSpeed")
  local mechWalkSlowSpeed = tech.parameter("mechWalkSlowSpeed")
  local mechSkiddingFriction = tech.parameter("mechSkiddingFriction")
  local mechBoostAirForce = tech.parameter("mechBoostAirForce")
  local mechLandingProjectile = tech.parameter("mechLandingProjectile")
  local mechLandingLocation = tech.parameter("mechLandingLocation")
  local mechStartupTime = tech.parameter("mechStartupTime")
  local mechShutdownTime = tech.parameter("mechShutdownTime")
  
  -- Jump, Dash, Boost
  local jumpDuration = tech.parameter("jumpDuration")
  local jumpSpeed = tech.parameter("jumpSpeed")
  local jumpControlForce = tech.parameter("jumpControlForce")
  local dashDuration = tech.parameter("dashDuration")
  local dashSpeed = tech.parameter("dashSpeed")
  local dashControlForce = tech.parameter("dashControlForce")
  local dashAngle = tech.parameter("dashAngle") * math.pi / 180
  local backHopDuration = tech.parameter("backHopDuration")
  local backHopSpeed = tech.parameter("backHopSpeed")
  local backHopControlForce = tech.parameter("backHopControlForce")
  local backHopAngle = tech.parameter("backHopAngle") * math.pi / 180
  local backHopCooldown = tech.parameter("backHopCooldown")
  local boostSpeed = tech.parameter("boostSpeed")
  local boostControlForce = tech.parameter("boostControlForce")
  
  -- Equipment
  local mechLeftEquip = tech.parameter("mechLeftEquip")
  local mechLeftAnimation = tech.parameter("mechLeftAnimation")
  local mechRightEquip = tech.parameter("mechRightEquip")
  local mechRightAnimation = tech.parameter("mechRightAnimation")
  local mechEquip = {mechLeftEquip, mechRightEquip}
  
  -- Weapon 1 Plasma Lathe
  local mechWeapon1Projectile = tech.parameter("mechWeapon1Projectile")
  local mechWeapon1ProjectileF = tech.parameter("mechWeapon1ProjectileF")
  local mechWeapon1Projectile2 = tech.parameter("mechWeapon1Projectile2")
  local mechWeapon1Projectile2F = tech.parameter("mechWeapon1Projectile2F")
  local mechWeapon1Projectile3 = tech.parameter("mechWeapon1Projectile3")
  local mechWeapon1Projectile4 = tech.parameter("mechWeapon1Projectile4")
  local mechWeapon1MinPower = tech.parameter("mechWeapon1MinPower")
  local mechWeapon1MaxPower = tech.parameter("mechWeapon1MaxPower")
  local mechWeapon1MaxEnergy = tech.parameter("mechWeapon1MaxEnergy") 
  local mechWeapon1Scalar = (mechWeapon1MaxPower - mechWeapon1MinPower)/mechWeapon1MaxEnergy
  local mechWeapon1DownSlashPowerMultiplier = tech.parameter("mechWeapon1DownSlashPowerMultiplier")
  local mechWeapon1WindupTime = tech.parameter("mechWeapon1WindupTime")
  local mechWeapon1SwingTime = tech.parameter("mechWeapon1SwingTime")
  local mechWeapon1ProjectileSpawnDelay = mechWeapon1SwingTime - tech.parameter("mechWeapon1ProjectileSpawnDelay")
  local mechWeapon1Projectile2SpawnDelay = mechWeapon1SwingTime - tech.parameter("mechWeapon1Projectile2SpawnDelay")
  local mechWeapon1WinddownTime = tech.parameter("mechWeapon1WinddownTime")  
  local mechWeapon1WindupTime2 = tech.parameter("mechWeapon1WindupTime2")
  local mechWeapon1SwingTime2 = tech.parameter("mechWeapon1SwingTime2")
  local mechWeapon1WinddownTime2 = tech.parameter("mechWeapon1WinddownTime2")    
  local mechWeapon1ResetTime = tech.parameter("mechWeapon1ResetTime")
  local mechWeapon1GracePeriod = tech.parameter("mechWeapon1GracePeriod")
  local mechWeapon1CooldownTime = tech.parameter("mechWeapon1CooldownTime")
  
  -- Weapon 2 Zappo Graviton Beam
  local mechWeapon2Mode = tech.parameter("mechWeapon2Mode")
  local mechWeapon2WarmupTime = tech.parameter("mechWeapon2WarmupTime")
  local mechWeapon2PullCoefficient = tech.parameter("mechWeapon2PullCoefficient")
  local mechWeapon2MaxRange = tech.parameter("mechWeapon2MaxRange")
  local mechWeapon2BeamCrosshair = tech.parameter("mechWeapon2BeamCrosshair")
  local mechWeapon2BeamProjectile= tech.parameter("mechWeapon2BeamProjectile")
  local mechWeapon2BeamIndicatorProjectile = tech.parameter("mechWeapon2BeamIndicatorProjectile")
  local mechWeapon2BeamStep= tech.parameter("mechWeapon2BeamStep")
  local mechWeapon2BeamUpdateTime = tech.parameter("mechWeapon2BeamUpdateTime")
  local mechWeapon2BeamEndProjectile = tech.parameter("mechWeapon2BeamEndProjectile")
  local mechWeapon2BeamStartProjectile = tech.parameter("mechWeapon2BeamStartProjectile")
  local mechWeapon2BeamStartRadius = tech.parameter("mechWeapon2BeamStartRadius")
  local mechWeapon2BeamIndicatorStartProjectile = tech.parameter("mechWeapon2BeamIndicatorStartProjectile")
  
  -- Weapon 3 Gun
  local mechAimLimit = tech.parameter("mechAimLimit") * math.pi / 180
  local mechFireCycle = tech.parameter("mechFireCycle")
  local mechProjectile = tech.parameter("mechProjectile")
  local mechProjectileConfig = tech.parameter("mechProjectileConfig")
  local mechGunFireCone = tech.parameter("mechGunFireCone") * math.pi / 180
  local mechGunRotationRadius = tech.parameter("mechGunRotationRadius")
  local mechCasingProjectile = tech.parameter("mechCasingProjectile")
  local mechGunChargeUpTime = tech.parameter("mechGunChargeUpTime")
  local mechGunCooldownTime = tech.parameter("mechGunCooldownTime")
  local mechGunRecoilSpeed = tech.parameter("mechGunRecoilSpeed")
  local mechGunRecoilPower = tech.parameter("mechGunRecoilPower")
  local mechGunRecoilKick = tech.parameter("mechGunRecoilKick") * math.pi / 180
  
  if not self.active and args.actions["mechActivate"] and self.mechState == "off" and mcontroller.onGround() then
    mechCollisionTest[1] = mechCollisionTest[1] + mcontroller.position()[1]
    mechCollisionTest[2] = mechCollisionTest[2] + mcontroller.position()[2]
    mechCollisionTest[3] = mechCollisionTest[3] + mcontroller.position()[1]
    mechCollisionTest[4] = mechCollisionTest[4] + mcontroller.position()[2]
    if (not world.rectCollision(mechCollisionTest)) then
      -- tech.burstParticleEmitter("mechActivateParticles")
      mcontroller.translate(mechTransformPositionChange)
      tech.setVisible(true)
	  tech.setAnimationState("movement", "idle")
      tech.setParentState("stand")
      tech.setToolUsageSuppressed(true)

	  self.mechState = "turningOn"
	  self.mechStateTimer = mechStartupTime
	  self.active = true
	  tech.playSound("mechStartupSound")
	  
	  local diff = world.distance(tech.aimPosition(), mcontroller.position())
      local aimAngle = math.atan2(diff[2], diff[1])
      self.mechFlipped = aimAngle > math.pi / 2 or aimAngle < -math.pi / 2
    else
      -- Make some kind of error noise
    end	  
  elseif self.mechState == "turningOn" and self.mechStateTimer <= 0 then
    self.mechState = "on"
	self.mechStateTimer = 0
  elseif (self.active and (args.actions["mechDeactivate"] and self.mechState == "on" and mcontroller.onGround()) or (energyCostPerSecond * args.dt > status.resource("energy")) and self.mechState == "on") then
	self.mechState = "turningOff"
	self.mechStateTimer = mechShutdownTime
	tech.playSound("mechShutdownSound")
  elseif self.mechState == "turningOff" and self.mechStateTimer <= 0 then 
	-- tech.burstParticleEmitter("mechDeactivateParticles")
	-- world.spawnProjectile("xsm_rhomechwarpout", mcontroller.position(), entity.id(), {0, 0}, false)
	
	tech.setVisible(false)
    tech.setParentState()  
    mcontroller.translate({-mechTransformPositionChange[1], -mechTransformPositionChange[2]})
    tech.setToolUsageSuppressed(false)
    tech.setParentOffset({0, 0})
	
    -- Activate/Deactivate
    self.mechState = "off"
    self.mechStateTimer = 0
	
    self.firePressed = false
    self.fireReleased = false
    self.altFirePressed = false
    self.altFireReleased = false

	-- Landing
	self.fastDrop = false
	self.veryFastDrop = false
	self.veryVeryFastDrop = false
	self.movementSuppressed = false
	self.immobileTimer = 0
	
	-- Jumping (short jump, dash, boost)
	self.isJumping = false
	self.hasJumped = false
	self.jumpPressed = false
	self.jumpReleased = false
	self.jumpTimer = 0
	self.jumpCoolDown = 0
	self.isDashing = false
	self.dashTimer = 0
    self.dashDirection = 0
    self.dashLastInput = 0
    self.dashTapLast = 0
    self.dashTapTimer = 0
	self.isBoosting = false
	self.boostTimer = 0
	self.backHopTimer = 0
	self.isCrouching = false
	
	-- Animation / Particles
	tech.setAnimationState("jetFlame", "off")
	tech.setAnimationState("movement", "idle")
	tech.setAnimationState("frontWeapon1", "off")
	tech.setAnimationState("backWeapon1", "off")
	tech.setAnimationState("frontWeapon2", "off")
	tech.setAnimationState("backWeapon2", "off")
	tech.setAnimationState("frontFiring", "off")
	tech.setAnimationState("backFiring", "off")
	tech.setAnimationState("frontArmMovement", "off")
	tech.setAnimationState("backArmMovement", "off")
	tech.setAnimationState("frontArmSwordMovement", "off")
	tech.setAnimationState("backArmSwordMovement", "off")
	tech.setAnimationState("frontArmGunMovement", "off")
	tech.setAnimationState("backArmGunMovement", "off")
	tech.setAnimationState("movement", "idle")
	tech.setAnimationState("torso", "idle")
	tech.setParticleEmitterActive("boostParticles", false)
	tech.setParticleEmitterActive("dashParticles", false)
	tech.setParticleEmitterActive("skidParticles", false)
	tech.setParticleEmitterActive("veryHardLandingParticles", false)
	tech.setParticleEmitterActive("hardLandingParticles", false)
	tech.setParticleEmitterActive("mechSteam2", false)
	tech.setParticleEmitterActive("mechSteam", false)
	
	-- Melee
    self.meleeTimer = 0
	self.oldMeleeTimer = 0
	self.forceWalk = false
	self.twohswing = false
	self.weaponState = "idle"
	self.attackQueued = false
	self.attacking = false
	self.projectilesSpawned = 0
	self.forceWalkMelee = false
    self.rightArmOccupied = false
    self.leftArmOccupied = false
    self.frontArmOccupied = false
    self.backArmOccupied = false
	
	-- Tractor beam
	self.entityQueryTable = {}
	self.itemExceptionTable = {}
    self.altWeaponState = "idle"
    self.altTimer = 0
	self.forceWalkBeam = false
	self.beamTimer = 0
	
	-- Machine Gun
	self.mGunTimer = 0
	self.mGunState = "idle"
	self.forceWalkGun = false
	self.mGunRecoilKick = 0
	
    self.active = false
  end

  mcontroller.controlFace(nil)
  
  if self.mechStateTimer > 0 then
	self.mechStateTimer = self.mechStateTimer - args.dt
  end

  local diff = world.distance(tech.aimPosition(), mcontroller.position())
  local aimAngle = math.atan2(diff[2], diff[1])
  local flip = aimAngle > math.pi / 2 or aimAngle < -math.pi / 2  

  -- Basic animation
  if self.active then
  
	mcontroller.controlParameters(mechCustomMovementParameters)
    
	-- Gun angle correction
	if table.exists("machineGun", mechEquip) and self.mechState == "on" then
	  local gunSide = nil
	  local gunRotationPosition = {}
	  if mechLeftEquip == "machineGun" then
		gunSide = "left"
	  elseif mechRightEquip == "machineGun" then
		gunSide = "right"
	  end
	  gunRotationPosition = vec_add(mcontroller.position(), mechChooseObject( tech.anchorPoint("frontGunRotationPoint"), tech.anchorPoint("backGunRotationPoint"), gunSide, self.mechFlipped))  
	  local radius = mechGunRotationRadius
	  local range = math.distance(tech.aimPosition(), gunRotationPosition)
	  local correction = math.asin( radius / range )
	  local diff2 = world.distance(tech.aimPosition(), gunRotationPosition)
	  local refAngle = math.atan2(diff2[2], diff2[1])
	  
	  -- Protection against gun wildly swinging at close range
	  if range <= 2 * radius then
	    correction = 0
	  end
	  
	  -- Apply correction
	  if self.mechFlipped then
	    aimAngle = refAngle - correction - self.mGunRecoilKick
	  else
	    aimAngle = refAngle + correction + self.mGunRecoilKick
	  end
	end

	if (not self.isDashing) and (not self.attacking) and self.mechState == "on" then
		if flip then  
		  self.mechFlipped = true
		else
		  self.mechFlipped = false
		end
	end	
	if self.mechFlipped then
	  tech.setFlipped(true)  
	  if self.rightArmOccupied or self.twohswing then
		self.backArmOccupied = true
	  else
		self.backArmOccupied = false
	  end
	  if self.leftArmOccupied or self.twohswing then
		self.frontArmOccupied = true
	  else
		self.frontArmOccupied = false
	  end				  
	  local nudge = tech.appliedOffset()
	  tech.setParentOffset({-parentOffset[1] - nudge[1], parentOffset[2] + nudge[2]})
	  mcontroller.controlFace(-1)	
      if aimAngle > 0 then
        aimAngle = math.max(aimAngle, math.pi - mechAimLimit)
      else
        aimAngle = math.min(aimAngle, -math.pi + mechAimLimit)
      end
	  if self.mechState == "on" then
	    tech.rotateGroup("guns", math.pi - aimAngle)
	  else
	    tech.rotateGroup("guns", 0)
	  end
	else
	  tech.setFlipped(false)
	  if self.rightArmOccupied or self.twohswing then
		self.frontArmOccupied = true
	  else
		self.frontArmOccupied = false
	  end
	  if self.leftArmOccupied or self.twohswing then
		self.backArmOccupied = true
	  else
		self.backArmOccupied = false
	  end	
	  local nudge = tech.appliedOffset()
	  tech.setParentOffset({parentOffset[1] + nudge[1], parentOffset[2] + nudge[2]})
	  mcontroller.controlFace(1)		
      if aimAngle > 0 then
        aimAngle = math.min(aimAngle, mechAimLimit)
      else
        aimAngle = math.max(aimAngle, -mechAimLimit)
      end	  
	  if self.mechState == "on" then
	    tech.rotateGroup("guns", aimAngle)
	  else
	    tech.rotateGroup("guns", 0)
	  end
	end  
  end
  
  -- Startup, shutdown, and active
  if self.mechState == "turningOn" then
	mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)
	tech.setAnimationState("movement", "stand")
	tech.setAnimationState("torso", "turnOn")
	mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "idle")
	mechAnimatePauldron("frontArmPauldronMovement", "idle")
  elseif self.mechState == "turningOff" then
	mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)
	tech.setAnimationState("movement", "sit")
	tech.setAnimationState("torso", "turnOff")	   
	mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "idle")
	mechAnimatePauldron("frontArmPauldronMovement", "idle")
  elseif self.active and self.mechState == "on" then
	
	-- Misc. Animation-related checks
	tech.setAnimationState("torso", "idle")
	  if mechLeftEquip == "machineGun" then
		mechSetArmOccupied ("left", true)
	  elseif mechRightEquip == "machineGun" then
		mechSetArmOccupied ("right", true)
	  end
	
	-- -- Equip guns (only gravGun arm can swap to a gun)
	-- local equipArm = nil
	-- if table.exists("gravGun", mechEquip) and self.altWeaponState == "idle" then	  
	  -- if world.entityHandItem(entity.id(), "primary") == "xsm_mechmachinegun" then
	    -- if mechLeftEquip == "gravGun" then
		  -- mechLeftEquip = "machineGun"
		  -- mechLeftAnimation = {"frontArmGunMovement", "backArmGunMovement"}
		  -- mechSetArmOccupied ("left", true)
		  -- equipArm = "left"
		-- elseif mechRightEquip == "gravGun" then
		  -- mechRightEquip = "machineGun"
		  -- mechRightAnimation = {"frontArmGunMovement", "backArmGunMovement"}
		  -- mechSetArmOccupied ("right", true)
		  -- equipArm = "right"
		-- end
		-- table.delete("gravGun", mechEquip)
		-- table.nrAdd("machineGun", mechEquip)
		-- -- Turn off grav gun animation
		-- tech.setAnimationState("frontArmMovement", "off")
		-- tech.setAnimationState("backArmMovement", "off")
		
		-- -- Other item checks
	  -- end
	-- elseif table.exists("machineGun", mechEquip) and world.entityHandItem(entity.id(), "primary") ~= "xsm_mechmachinegun" then		  
	  -- if mechLeftEquip == "machineGun" then
		  -- equipArm = "left"
	  -- elseif mechRightEquip == "machineGun" then
		  -- equipArm = "right"
	  -- end	
	  -- mechAnimateActiveArm("frontArmMovement", "backArmMovement", "idle", equipArm, self.mechFlipped)
	  -- table.delete("machineGun", mechEquip)

	  -- tech.setAnimationState("frontArmGunMovement", "off")
	  -- tech.setAnimationState("backArmGunMovement", "off")
	  -- mech.animateActiveArm
	-- end
	
	-- State checks, animation
	self.isCrouching = false
    if not mcontroller.onGround() then
      if mcontroller.velocity()[2] > 0 then
	    -- Jump animation only
        tech.setAnimationState("movement", "jump")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "jump")
		mechAnimatePauldron("frontArmPauldronMovement", "jump")
		self.fastDrop = false
	    self.veryFastDrop = false
      elseif not self.movementSuppressed then
	    -- Falling animation. Hard landings and very hard landings suppress movement temporarily.
		if not self.hasJumped then
	      self.jumpReleased = true
		end
        tech.setAnimationState("movement", "fall")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "fall")
		mechAnimatePauldron("frontArmPauldronMovement", "fall")
		if (mcontroller.velocity()[2] < -30 and mcontroller.velocity()[2] > -52) or math.abs(mcontroller.velocity()[1]) > 30 then
		  self.fastDrop = true
		elseif mcontroller.velocity()[2] <= -52 and mcontroller.velocity()[2] > -68 then
		  self.fastDrop = false
		  self.veryFastDrop = true
		elseif mcontroller.velocity()[2] <= -68 then
		  self.fastDrop = false
		  self.veryFastDrop = false
		  self.veryVeryFastDrop = true
		end	
      end
	  if self.holdingJump and not self.isBoosting and self.jumpReleased and not self.isDashing and not self.holdingDown and status.resource("energy") > (energyCostPerBoost + energyCostPerSecondBoost * 0.4) then
	    -- Initiate boosting
		self.jumpReleased = false	    
		self.isBoosting = true
		tech.playSound("boostInitiateSound")
		energyCostAccumulated = energyCostAccumulated + energyCostPerBoost
	  end
	else
	  self.jumpReleased = false
	  self.hasJumped = false
		if self.fastDrop and not self.movementSuppressed then
		  -- Hard landing
		  self.immobileTimer = 0.15 -- TEMP
		  self.movementSuppressed = true	  
		  tech.burstParticleEmitter("hardLandingParticles")
		elseif self.veryFastDrop and not self.movementSuppressed then
		  -- Very hard landing	  
		  self.immobileTimer = 0.66 -- TEMP
		  self.movementSuppressed = true
		  tech.burstParticleEmitter("veryHardLandingParticles")
		elseif self.veryVeryFastDrop and not self.movementSuppressed then
		  -- Very, Very hard landing
		  self.immobileTimer = 0.78 -- TEMP
		  self.movementSuppressed = true
		  tech.burstParticleEmitter("veryHardLandingParticles")	
		  local particlePosition = {mcontroller.position()[1] + mechLandingLocation[1], mcontroller.position()[2] + mechLandingLocation[2]}
		  world.spawnProjectile(mechLandingProjectile, particlePosition, entity.id(), {0, 0}, false)
		elseif self.holdingJump and not self.isJumping and not self.movementSuppressed and not self.holdingDown then
		  -- Activate hop (jumping)
		  self.isJumping = true
		  self.jumpTimer = jumpDuration
		  tech.playSound("jumpSound")
		  self.hasJumped = true
		elseif (mcontroller.walking() or mcontroller.running()) and not self.movementSuppressed then
		  -- walk/run/backWalk/walkSlow/backWalkSlow animations
		  self.fastDrop = false
		  self.veryFastDrop = false
		  local mechCustomHopMovementParameters = copy(tech.parameter("mechCustomMovementParameters"))
		  if self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1 then
		    if self.forceWalk then
			  tech.setAnimationState("movement", "backWalkSlow")
			  mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "backWalkSlow")
			  mechAnimatePauldron("frontArmPauldronMovement", "backWalkSlow")
			  mechCustomHopMovementParameters.runSpeed = mechWalkSlowSpeed
			  mechCustomHopMovementParameters.walkSpeed = mechWalkSlowSpeed
			  mcontroller.controlParameters(mechCustomHopMovementParameters)	
			else  
			  tech.setAnimationState("movement", "backWalk")
		      mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "backWalk")
			  mechAnimatePauldron("frontArmPauldronMovement", "backWalk")
			  mechCustomHopMovementParameters.runSpeed = mechBackwardSpeed
			  mechCustomHopMovementParameters.walkSpeed = mechBackwardSpeed
			  mcontroller.controlParameters(mechCustomHopMovementParameters)
			end
		  else
		    if self.forceWalk then
			  tech.setAnimationState("movement", "walkSlow")
			  mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "walkSlow")
			  mechAnimatePauldron("frontArmPauldronMovement", "walkSlow")
			  mechCustomHopMovementParameters.runSpeed = mechWalkSlowSpeed
			  mechCustomHopMovementParameters.walkSpeed = mechWalkSlowSpeed
			  mcontroller.controlParameters(mechCustomHopMovementParameters)		
			else
			  if mcontroller.running() then
			    tech.setAnimationState("movement", "walk")
			    mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "walk")
				mechAnimatePauldron("frontArmPauldronMovement", "walk")
			  elseif mcontroller.walking() then
			    tech.setAnimationState("movement", "run")
				tech.burstParticleEmitter("mechSteam2", true)
			    mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "run")
				mechAnimatePauldron("frontArmPauldronMovement", "run")
				energyCostAccumulated = energyCostAccumulated + energyCostPerSecondRunning * args.dt
			  end
			end
		  end
		elseif not self.movementSuppressed then
		  -- Idle
		  self.fastDrop = false
		  self.veryFastDrop = false
		  mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "idle")
		  mechAnimatePauldron("frontArmPauldronMovement", "idle")
			-- Crouching
		  if self.holdingDown then
			tech.setAnimationState("movement", "sit")
			self.isCrouching = true
		  else
		    tech.setAnimationState("movement", "idle")
		  end
		end
	end
	
	-- Temp fall protection
	if mcontroller.velocity()[2] < -70 then
		mcontroller.controlApproachYVelocity(-70, 6000, false)
	end
	
	-- Temp Air friction
	if math.abs(mcontroller.velocity()[1]) > 30 then
	  mcontroller.controlApproachXVelocity(0, 100, true)
	end
	
	-- Jumping physics
	if self.isJumping and self.jumpTimer > 0 and self.holdingJump and not self.movementSuppressed then
	  mcontroller.controlApproachYVelocity(jumpSpeed, jumpControlForce, true)
	  self.jumpTimer = self.jumpTimer - args.dt
	elseif self.isJumping then
	  self.isJumping = false
	  self.jumpTimer = 0
	end
	
	-- Dashing physics
	if args.actions["dashRight"] and self.dashTimer <= 0 and not self.movementSuppressed then
	  if (self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1) and mcontroller.onGround() and status.resource("energy") > energyCostPerBackHop then
	    if self.backHopTimer <= 0 then
		  self.dashTimer = backHopDuration
	      self.dashDirection = 1	  
		  energyCostAccumulated = energyCostAccumulated + energyCostPerBackHop
		  tech.playSound("backHopSound")
		  self.backHopTimer = backHopCooldown
		end
	  elseif mcontroller.onGround() and status.resource("energy") > energyCostPerDash then
	    self.dashTimer = dashDuration
	    self.dashDirection = 2
		energyCostAccumulated = energyCostAccumulated + energyCostPerDash
		tech.playSound("jumpSound")
		tech.playSound("dashSound")
	  elseif not (self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1) and status.resource("energy") > energyCostPerDash then
	    self.dashTimer = dashDuration
	    self.dashDirection = 3
		energyCostAccumulated = energyCostAccumulated + energyCostPerDash
		tech.playSound("dashSound")
	  end
	  self.isDashing = true
	  self.jumpReleased = true
	elseif args.actions["dashLeft"] and self.dashTimer <= 0 and not self.movementSuppressed then
	  if (self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1) and mcontroller.onGround() and status.resource("energy") > energyCostPerBackHop then
	    if self.backHopTimer <= 0 then
		  self.dashTimer = backHopDuration
	      self.dashDirection = -1	 
		  energyCostAccumulated = energyCostAccumulated + energyCostPerBackHop
		  tech.playSound("backHopSound")
		  self.backHopTimer = backHopCooldown
		end
	  elseif mcontroller.onGround() and status.resource("energy") > energyCostPerDash then  
	    self.dashTimer = dashDuration
	    self.dashDirection = -2
		energyCostAccumulated = energyCostAccumulated + energyCostPerDash
		tech.playSound("jumpSound")
		tech.playSound("dashSound")
	  elseif not (self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1) and status.resource("energy") > energyCostPerDash then
	    self.dashTimer = dashDuration
	    self.dashDirection = -3
		energyCostAccumulated = energyCostAccumulated + energyCostPerDash 
		tech.playSound("dashSound")
	  end
	  self.isDashing = true
	  self.jumpReleased = true
	end
	if self.dashTimer > 0 then
	  -- Backhop
	  if self.dashDirection == 1 then
		local diagX = math.cos(backHopAngle)
		local diagY = math.sin(backHopAngle)
		mcontroller.controlApproachVelocity({backHopSpeed * diagX, backHopSpeed * diagY}, backHopControlForce, true, true)	
		-- Backhop right animation + particles
	  elseif self.dashDirection == -1 then
		local diagX = -math.cos(backHopAngle)
		local diagY = math.sin(backHopAngle)
		mcontroller.controlApproachVelocity({backHopSpeed * diagX, backHopSpeed * diagY}, backHopControlForce, true, true)	
		-- Backhop left animation + particles  

	  -- Full Power Dash
	  elseif self.dashDirection == 2 then
		local diagX = math.cos(dashAngle)
		local diagY = math.sin(dashAngle)
		mcontroller.controlApproachVelocity({dashSpeed * diagX, dashSpeed * diagY}, dashControlForce, true, true)
		tech.setAnimationState("jetPack", "boost3")
		tech.setAnimationState("jetFlame", "dash")
		tech.setParticleEmitterActive("dashParticles", true)
	  elseif self.dashDirection == -2 then
		local diagX = -math.cos(dashAngle)
		local diagY = math.sin(dashAngle)
		mcontroller.controlApproachVelocity({dashSpeed * diagX, dashSpeed * diagY}, dashControlForce, true, true)	
		tech.setAnimationState("jetPack", "boost3")
		tech.setAnimationState("jetFlame", "dash")
		tech.setParticleEmitterActive("dashParticles", true)
		
	  -- Full Power Dash (in Midair)
	  elseif self.dashDirection == 3 then
		mcontroller.controlApproachXVelocity(dashSpeed, dashControlForce, true)
		tech.setAnimationState("jetPack", "boost3")
		tech.setAnimationState("jetFlame", "dash")
		tech.setParticleEmitterActive("dashParticles", true)
	  elseif self.dashDirection == -3 then
		mcontroller.controlApproachXVelocity(-dashSpeed, dashControlForce, true)	
		tech.setAnimationState("jetPack", "boost3")
		tech.setAnimationState("jetFlame", "dash")
		tech.setParticleEmitterActive("dashParticles", true)  
	  end  
	  self.dashTimer = self.dashTimer - args.dt	  
	elseif self.isDashing then
	  self.isDashing = false
	  self.dashTimer = 0
	  tech.setAnimationState("jetPack", "turnoff3")
	  tech.setAnimationState("jetFlame", "off")
	  tech.setParticleEmitterActive("dashParticles", false)
	  self.jumpReleased = true
	end
	
	-- Boosting physics (Jetpack) - Dash overrides this
	if self.isBoosting and self.holdingJump and not self.isDashing and status.resource("energy") > ( energyCostPerSecondBoost * 0.4 ) then
	  mcontroller.controlApproachYVelocity(boostSpeed, boostControlForce, true)
	  local mechCustomJetpackMovementParameters = copy(tech.parameter("mechCustomMovementParameters"))
	  mechCustomJetpackMovementParameters.airForce = mechBoostAirForce
	  mcontroller.controlParameters(mechCustomJetpackMovementParameters)
	  tech.setAnimationState("jetPack", "boost1")
	  tech.setAnimationState("jetFlame", "boost")
	  tech.setParticleEmitterActive("boostParticles", true)
	  energyCostAccumulated = energyCostAccumulated + energyCostPerSecondBoost * args.dt
	  self.boostTimer = self.boostTimer + args.dt
	elseif self.isBoosting then
	  self.isBoosting = false
	  self.boostTimer = 0	
	  tech.setAnimationState("jetPack", "turnoff1")
	  tech.setAnimationState("jetFlame", "off")
	  tech.setParticleEmitterActive("boostParticles", false)
	end

	-- Landing Movement Suppression
	if self.immobileTimer > 0 and mcontroller.onGround() then
	  self.immobileTimer = self.immobileTimer - args.dt  
	  -- Skidding
	  local mechCustomSkiddingMovementParameters = copy(tech.parameter("mechCustomMovementParameters"))
	  if math.abs(mcontroller.velocity()[1]) < 15 then
		mcontroller.controlApproachXVelocity(0, 3000, true)
		tech.setParticleEmitterActive("skidParticles", false)
	  elseif math.abs(mcontroller.velocity()[1]) < 30 then
	    -- do nothing
		tech.setParticleEmitterActive("skidParticles", false)
	  else
	    mechCustomSkiddingMovementParameters.normalGroundFriction = mechSkiddingFriction
	    mcontroller.controlParameters(mechCustomSkiddingMovementParameters) -- dynamic coefficient of friction, y'see....
		tech.setParticleEmitterActive("skidParticles", true)
		-- Spawn dust
	  end	  
	  -- Animation
	  if self.veryFastDrop or self.veryVeryFastDrop then
		tech.setAnimationState("movement", "landHard")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "landHard")	
		mechAnimatePauldron("frontArmPauldronMovement", "landHard")
	  elseif self.fastDrop then
		tech.setAnimationState("movement", "land")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "land")	
		mechAnimatePauldron("frontArmPauldronMovement", "land")
	  end
	  mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)	  
	elseif (self.movementSuppressed and self.immobileTimer <= 0) or (self.immobileTimer > 0 and not mcontroller.onGround()) then
	  tech.setParticleEmitterActive("skidParticles", false)
	  self.movementSuppressed = false
	  self.immobileTimer = 0
	  self.fastDrop = false
	  self.veryFastDrop = false
	  self.veryVeryFastDrop = false
	end
	
	-- Force walking (when attacking with melee or beam)
	if self.forceWalkMelee or self.forceWalkBeam or self.forceWalkGun then
	  self.forceWalk = true
	else
	  self.forceWalk = false
	end
	
	-- Timers
	if self.meleeTimer > 0 then
	  self.meleeTimer = self.meleeTimer - args.dt
	end
	if self.altTimer > 0 then
	  self.altTimer = self.altTimer - args.dt
	end
	if self.backHopTimer > 0 then
	  self.backHopTimer = self.backHopTimer - args.dt
	end
	if self.beamTimer > 0 then
	  self.beamTimer = self.beamTimer - args.dt
	end
	if self.mGunTimer > 0 then
	  self.mGunTimer = self.mGunTimer - args.dt
	end
	
	-- STANDARD WEAPON: Plasma Lathe. A one-handed plasma lathe (a sword built into the mech's arm). Damage scales with energy remaining.
	if table.exists("sword", mechEquip) then
	
	  local mechActiveSide = nil
	  local mechOtherSide = nil
	  local mechActiveAnim = {}
	  local mechOtherAnim = {}
	  local mechActiveSideButton = nil
	  local mechActiveSideButtonRelease = nil
	  
	  if mechLeftEquip == "sword" then
	    mechActiveSide = "left"
		mechOtherSide = "right"
		mechActiveAnim = mechLeftAnimation
		mechOtherAnim = mechRightAnimation
		mechActiveSideButton = self.holdingFire
		mechActiveSideButtonRelease = self.fireReleased
	  elseif mechRightEquip == "sword" then
	    mechActiveSide = "right"
		mechOtherSide = "left"
		mechActiveAnim = mechRightAnimation
		mechOtherAnim = mechLeftAnimation
		mechActiveSideButton = self.holdingAltFire
		mechActiveSideButtonRelease = self.altFireReleased
	  end
	  
	  if mechActiveSideButtonRelease and mechActiveSideButton and not self.attackQueued then
	    self.attackQueued = true
		releaseInput(mechActiveSide)
	  end
	  
	  if (self.weaponState == "slashUp_windup" or self.weaponState == "slashUp" or self.weaponState == "slashUp_winddown" or self.weaponState == "slashUp_reset" or self.weaponState == "slashDown_windup" or self.weaponState == "slashDown" or self.weaponState == "slashDown_winddown" or self.weaponState == "slashDown_reset") then
	    self.attacking = true
	  else
	    self.attacking = false
	  end  
	  	  
	  -- Primary attack
	  if (mechActiveSideButton or self.attackQueued) and self.meleeTimer <= 0 and self.weaponState == "idle" and status.resource("energy") > energyCostPerSlash then
		-- Slash Up Wind Up
	    mechSetArmOccupied (mechActiveSide, true)
		self.forceWalkMelee = true	
	    self.attackQueued = false
		releaseInput(mechActiveSide)			    
		tech.playSound("mechWeapon1IgniteSound")		
	    self.weaponState = "slashUp_windup"
		self.meleeTimer = mechWeapon1WindupTime
	  elseif self.weaponState == "slashUp_windup" and self.meleeTimer <= 0 then
	    -- Slash Up
	    self.weaponState = "slashUp"
		self.meleeTimer = mechWeapon1SwingTime
		energyCostAccumulated = energyCostAccumulated + energyCostPerSlash
		tech.playSound("mechWeapon1SwingSound")
		local firePoint = vec_add(mcontroller.position(), mechChooseObject(tech.anchorPoint("frontWeaponFirePoint"), tech.anchorPoint("backWeaponFirePoint"), mechActiveSide, self.mechFlipped))
		world.spawnProjectile(mechWeapon1Projectile4, firePoint, entity.id(), {0, 0}, false)		
	  elseif self.weaponState == "slashUp" and self.meleeTimer > 0 then
		if self.oldMeleeTimer > mechWeapon1ProjectileSpawnDelay and self.meleeTimer <= mechWeapon1ProjectileSpawnDelay then  
		  -- Slash Up projectiles
		  local firePoint = vec_add(mcontroller.position(), mechChooseObject(tech.anchorPoint("frontWeaponFirePoint"), tech.anchorPoint("backWeaponFirePoint"), mechActiveSide, self.mechFlipped))
		  local mechProjectile = mechChooseObject(mechWeapon1Projectile, mechWeapon1ProjectileF, nil, self.mechFlipped)
		  local slashPower = math.min(status.resource("energy") * mechWeapon1Scalar, mechWeapon1MaxPower)
		  world.spawnProjectile(mechProjectile, firePoint, entity.id(), {0, 0}, false, {power = slashPower})
		  world.spawnProjectile(mechWeapon1Projectile3, firePoint, entity.id(), {0, 0}, false)
		end
		self.oldMeleeTimer = self.meleeTimer		
		-- Spawn any projectiles particles etc
	  elseif self.weaponState == "slashUp" and self.meleeTimer <= 0 then
	    -- Slash Up Wind Down
	    self.weaponState = "slashUp_winddown"
		self.meleeTimer = mechWeapon1WinddownTime
	  elseif self.weaponState == "slashUp_winddown" and self.meleeTimer <= 0 then
	    -- Limbo, can turn around here
	    self.weaponState = "limbo"
		self.meleeTimer = mechWeapon1GracePeriod
	  elseif self.weaponState == "limbo" and self.meleeTimer <= 0 then -- and not self.attackQueued then
	    -- Slash Up Reset
	    self.weaponState = "slashUp_reset"
		self.meleeTimer = mechWeapon1ResetTime 
	  elseif (self.weaponState == "limbo" or self.weaponState == "slashUp_reset") and self.attackQueued and status.resource("energy") > energyCostPerSlash then
	    -- Slash Down Wind Up
	    self.attackQueued = false
		releaseInput(mechActiveSide)
	    self.weaponState = "slashDown_windup"
		self.meleeTimer = mechWeapon1WindupTime2
	  elseif self.weaponState == "slashDown_windup" and self.meleeTimer <= 0 then
	    -- Slash Down
	    self.weaponState = "slashDown"
		self.meleeTimer = mechWeapon1SwingTime2
		energyCostAccumulated = energyCostAccumulated + energyCostPerSlash	
		tech.playSound("mechWeapon1SwingSound2")
		local firePoint = vec_add(mcontroller.position(), mechChooseObject(tech.anchorPoint("frontWeaponFirePoint"), tech.anchorPoint("backWeaponFirePoint"), mechActiveSide, self.mechFlipped))
		world.spawnProjectile(mechWeapon1Projectile4, firePoint, entity.id(), {0, 0}, false)
	  elseif self.weaponState == "slashDown" and self.meleeTimer > 0 then
		if self.oldMeleeTimer > mechWeapon1Projectile2SpawnDelay and self.meleeTimer <= mechWeapon1Projectile2SpawnDelay then  
		  -- Slash Down projectiles
		  local firePoint = vec_add(mcontroller.position(), mechChooseObject(tech.anchorPoint("frontWeaponFirePoint"), tech.anchorPoint("backWeaponFirePoint"), mechActiveSide, self.mechFlipped))
		  local mechProjectile = mechChooseObject(mechWeapon1Projectile2, mechWeapon1Projectile2F, nil, self.mechFlipped)
		  local slashPower = math.min(status.resource("energy") * mechWeapon1Scalar, mechWeapon1MaxPower)
		  world.spawnProjectile(mechProjectile, firePoint, entity.id(), {0, 0}, false, {power = slashPower})
		  world.spawnProjectile(mechWeapon1Projectile3, firePoint, entity.id(), {0, 0}, false)
		end
		self.oldMeleeTimer = self.meleeTimer		
	  elseif self.weaponState == "slashDown" and self.meleeTimer <= 0 then
	    -- Slash Down Wind Down
	    self.weaponState = "slashDown_winddown"
		self.meleeTimer = mechWeapon1WinddownTime2
	  elseif self.weaponState == "slashDown_winddown" and self.meleeTimer <= 0 then
	    -- Limbo 2, can turn around here
	    self.weaponState = "limbo2"
		self.meleeTimer = mechWeapon1GracePeriod
	  elseif self.weaponState == "limbo2" and self.meleeTimer <= 0 then
	    -- Slash Down Reset
	    self.weaponState = "slashDown_reset"
		self.meleeTimer = mechWeapon1ResetTime
	  elseif (self.weaponState == "slashDown_reset" or self.weaponState == "limbo2") and self.attackQueued and status.resource("energy") > energyCostPerSlash then
	    -- Slash Up Wind Up (for combo)
	    self.attackQueued = false
		releaseInput(mechActiveSide)
	    self.weaponState = "slashUp_windup"
		self.meleeTimer = mechWeapon1WindupTime
	  elseif (self.weaponState == "slashDown_reset" or self.weaponState == "slashUp_reset") and self.meleeTimer <= 0 then
	    -- Cooldown
	    tech.playSound("mechWeapon1TurnOffSound")
	    self.weaponState = "cooldown"
		self.meleeTimer = mechWeapon1CooldownTime	
	    mechSetArmOccupied (mechActiveSide, false)
		self.forceWalkMelee = false
		self.attackQueued = false
		releaseInput(mechActiveSide)		
	  elseif self.weaponState == "cooldown" and self.meleeTimer <= 0 then
	    -- Set Idle
		self.weaponState = "idle"		
		self.meleeTimer = 0
	  elseif self.weaponState == "idle" then
	    -- Idle State
		-- Do nothing
	  end
	  
	  -- Animation
		if self.weaponState == "slashUp_windup" then
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "slashIdleDown", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)		
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "slashIdleDown", mechActiveSide, self.mechFlipped)
		elseif self.weaponState == "slashUp" then
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "slashUp", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "slashUp", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)	
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "slashUp", mechActiveSide, self.mechFlipped)
		elseif self.weaponState == "slashUp_winddown" then
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "slashIdleUp", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)	
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "slashIdleUp", mechActiveSide, self.mechFlipped)
		elseif self.weaponState == "limbo" then
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "slashIdleUp", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)	
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "slashIdleUp", mechActiveSide, self.mechFlipped)
		elseif self.weaponState == "slashUp_reset" then
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "resetSlash", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "resetSlash", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)	
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "resetSlash", mechActiveSide, self.mechFlipped)
		elseif self.weaponState == "slashDown_windup" then
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "slashIdleUp", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)		
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "slashIdleUp", mechActiveSide, self.mechFlipped)
		elseif self.weaponState == "slashDown" then
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "slashDown", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "slashDown", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)	
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "slashDown", mechActiveSide, self.mechFlipped)
		elseif self.weaponState == "slashDown_winddown" then
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "slashIdleDown", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "slashIdleDown", mechActiveSide, self.mechFlipped)
		elseif self.weaponState == "limbo2" then
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "slashIdleDown", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)	
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "slashIdleDown", mechActiveSide, self.mechFlipped)
		elseif self.weaponState == "slashDown_reset" then
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "resetSlashDown", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		  mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "resetSlashDown", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)		
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "resetSlashDown", mechActiveSide, self.mechFlipped)
		elseif self.weaponState == "cooldown" then
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)
		elseif self.weaponState == "idle" then
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontWeapon1", "backWeapon1", "off", mechOtherSide, self.mechFlipped)				
		end  
	end
	
	
	
	-- WEAPON: "Zappo" Graviton Beam. Can pull enemies to a set point, or alternatively, to the mouse cursor.
	if table.exists("gravGun", mechEquip) then
	
	  local mechActiveSide = nil
	  local mechOtherSide = nil
	  local mechActiveAnim = {}
	  local mechOtherAnim = {}
	  local mechActiveSideButton = nil
	  
	  if mechLeftEquip == "gravGun" then
	    mechActiveSide = "left"
		mechOtherSide = "right"
		mechActiveAnim = mechLeftAnimation
		mechOtherAnim = mechRightAnimation
		mechActiveSideButton = self.holdingFire
	  elseif mechRightEquip == "gravGun" then
	    mechActiveSide = "right"
		mechOtherSide = "left"
		mechActiveAnim = mechRightAnimation
		mechOtherAnim = mechLeftAnimation
		mechActiveSideButton = self.holdingAltFire
	  end
	
	  local pullTo = vec_add(mcontroller.position(), mechChooseObject(tech.anchorPoint("frontGravFirePoint"), tech.anchorPoint("backGravFirePoint"), mechActiveSide, self.mechFlipped))
	  if self.altWeaponState == "idle" and mechActiveSideButton then
	  -- Warm up
	    mechSetArmOccupied (mechActiveSide, true)
		self.forceWalkBeam = true				    
		tech.playSound("mechWeapon2WarmupSound")		
	    self.altWeaponState = "warmup"
		self.altTimer = mechWeapon2WarmupTime		
	  elseif self.altWeaponState == "warmup" and self.altTimer <= 0 and mechActiveSideButton then
		self.altWeaponState = "firing"
		tech.playSound("mechWeapon2FireSound")
	  elseif self.altWeaponState == "firing" and mechActiveSideButton then	  
		if self.beamTimer <= 0 then
		  local endPoint = {0, 0}
		  local endPointCollisionTest = progressiveLineCollision(pullTo, tech.aimPosition(), mechWeapon2BeamStep)
		    if math.distance(pullTo, endPointCollisionTest) <= mechWeapon2MaxRange then
			  endPoint = endPointCollisionTest
			else
			  local vec = math.getDir(pullTo, tech.aimPosition())
			  endPoint = { vec[1] * mechWeapon2MaxRange + pullTo[1], vec[2] * mechWeapon2MaxRange + pullTo[2] }
			end
		  progressiveLineCollision(pullTo, endPoint, mechWeapon2BeamStep, mechWeapon2BeamIndicatorProjectile)
		  world.spawnProjectile(mechWeapon2BeamIndicatorStartProjectile, pullTo, entity.id(), {0, 0}, false)
		  world.spawnProjectile(mechWeapon2BeamCrosshair, endPoint, entity.id(), {0, 0}, false)
		  self.beamTimer = mechWeapon2BeamUpdateTime
		end
	  
	  -- Find a target
	    local entityTable = world.entityQuery(tech.aimPosition(), 4)
	    if #entityTable > 0 then
	      for _, v in ipairs(entityTable) do
			-- if world.entityType(v) == "itemdrop" and (not table.exists(v, self.itemExceptionTable)) and (not world.lineTileCollision(pullTo, world.entityPosition(v)), "Dynamic") then
			  -- self.itemExceptionTable[#self.itemExceptionTable + 1] = v
			  -- local q = entity.id() --= world.playerQuery(mcontroller.position(), 4)
			    -- world.logInfo("%s", q)
			  -- local u = world.takeItemDrop(v, q)
			  -- if u == nil then
			    -- world.logInfo("%s", "XSM nil")
			  -- else
			    -- world.logInfo("%s", u)
			  -- end
			-- end	
			
		    if not table.exists(v, self.entityQueryTable) then	
			  -- With world.entityType(v) == "player", the Mech has the rather odd ability to grab itself. Not that it does anything.		  
			  if (world.entityType(v) == "monster" or world.entityType(v) == "npc") and (not world.lineTileCollision(pullTo, world.entityPosition(v), "Dynamic"))
			    and math.distance(pullTo, world.entityPosition(v)) <= mechWeapon2MaxRange and status.resource("energy") > (energyCostPerSecondPull * 0.5) then
			    self.entityQueryTable[#self.entityQueryTable + 1] = v
				self.altWeaponState = "locked"
				tech.playSound("mechWeapon2LockedSound")
				break -- Because we can only grab one at a time :D or else it would be OP D:
			  end
		    end	  
	      end
	    end
	  elseif self.altWeaponState == "locked" and mechActiveSideButton then
	  -- Pull Target	  
	    for _, v in ipairs(self.entityQueryTable) do
	      if world.entityExists(v) and not world.lineTileCollision(pullTo, world.entityPosition(v), "Dynamic") and math.distance(pullTo, world.entityPosition(v)) <= mechWeapon2MaxRange
		    and status.resource("energy") > (energyCostPerSecondPull * 0.2) then		
			local diffX = (pullTo[1] - world.entityPosition(v)[1]) 
			local diffY = (pullTo[2] - world.entityPosition(v)[2])
			if mechWeapon2Mode == "control" then
			  diffX = (tech.aimPosition()[1] - world.entityPosition(v)[1])
			  diffY = (tech.aimPosition()[2] - world.entityPosition(v)[2])
			end		
		    local velocity = {mechWeapon2PullCoefficient * diffX, mechWeapon2PullCoefficient * diffY}		  
		    world.callScriptedEntity(v, "mcontroller.setVelocity", velocity)
			if self.beamTimer <= 0 then
				local vec = math.getDir(pullTo, world.entityPosition(v))
				local pullToOffset = { vec[1] * mechWeapon2BeamStartRadius + pullTo[1], vec[2] * mechWeapon2BeamStartRadius + pullTo[2] }
				local endPoint = progressiveLineCollision(pullToOffset, world.entityPosition(v), mechWeapon2BeamStep, mechWeapon2BeamProjectile)
				world.spawnProjectile(mechWeapon2BeamStartProjectile, pullTo, entity.id(), {0, 0}, false)
				world.spawnProjectile(mechWeapon2BeamEndProjectile, endPoint, entity.id(), {0, 0}, false)
				self.beamTimer = mechWeapon2BeamUpdateTime
			end
		  else
		    table.remove(self.entityQueryTable, _)
		  end
	    end
		-- Energy Cost
		energyCostAccumulated = energyCostAccumulated + energyCostPerSecondPull * args.dt
	    -- Cancel
		if #self.entityQueryTable <= 0 then
		  self.altWeaponState = "firing"
		end			
	  elseif (self.altWeaponState == "firing" or self.altWeaponState == "locked" or self.altWeaponState == "warmup") and not mechActiveSideButton then
	  -- Cooldown
	    self.entityQueryTable = {}
		self.itemExceptionTable = {}
	    self.altWeaponState = "cooldown"
		self.altTimer = mechWeapon2WarmupTime
		self.forceWalkBeam = false
	  elseif self.altWeaponState == "cooldown" and self.altTimer <= 0 then
	  -- Set Idle
	    self.altWeaponState = "idle"
		mechSetArmOccupied (mechActiveSide, false)
		tech.playSound("mechWeapon2EndSound")
	  elseif self.altWeaponState == "idle" then
	    -- Idle; do nada
	  end
	  
	  -- Animation/Particles
	  if self.altWeaponState == "warmup" then
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "gravOn", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontArmPauldronMovement", nil, "gravOn", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontWeapon2", "backWeapon2", "off", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontWeapon2", "backWeapon2", "off", mechOtherSide, self.mechFlipped)	
	  elseif self.altWeaponState == "firing" then
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "gravFire", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontArmPauldronMovement", nil, "gravFire", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontWeapon2", "backWeapon2", "gravFire", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontWeapon2", "backWeapon2", "off", mechOtherSide, self.mechFlipped)	
		tech.setParticleEmitterActive("mechSteam", false)
	  elseif self.altWeaponState == "locked" then
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "gravLock", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontArmPauldronMovement", nil, "gravLock", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontWeapon2", "backWeapon2", "gravLock", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontWeapon2", "backWeapon2", "off", mechOtherSide, self.mechFlipped)	
		tech.setParticleEmitterActive("mechSteam", true)
	  elseif self.altWeaponState == "cooldown" then
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "gravOff", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)	
		mechAnimateActiveArm("frontArmPauldronMovement", nil, "gravOff", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontWeapon2", "backWeapon2", "off", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontWeapon2", "backWeapon2", "off", mechOtherSide, self.mechFlipped)	
		tech.setParticleEmitterActive("mechSteam", false)
	  end
	end

	-- WEAPON: Heavy machine gun
	if table.exists("machineGun", mechEquip) then
	
	  local mechActiveSide = nil
	  local mechOtherSide = nil
	  local mechActiveAnim = {}
	  local mechOtherAnim = {}
	  local mechActiveSideButton = nil
	  
	  if mechLeftEquip == "machineGun" then
	    mechActiveSide = "left"
		mechOtherSide = "right"
		mechActiveAnim = mechLeftAnimation
		mechOtherAnim = mechRightAnimation
		mechActiveSideButton = self.holdingFire
	  elseif mechRightEquip == "machineGun" then
	    mechActiveSide = "right"
		mechOtherSide = "left"
		mechActiveAnim = mechRightAnimation
		mechOtherAnim = mechLeftAnimation
		mechActiveSideButton = self.holdingAltFire
	  end  
	  
	  -- Firing
	  if mechActiveSideButton and self.mGunState == "idle" and status.resource("energy") > energyCostPerBullet + 1 then
	    self.mGunState = "chargeUp"
		self.forceWalkGun = true
		self.mGunTimer = mechGunChargeUpTime
		tech.playSound("mechGunChargeUpSound")
	  elseif self.mGunState == "chargeUp" and self.mGunTimer <= 0 then
		if mechActiveSideButton and status.resource("energy") > energyCostPerBullet + 1 then
		  self.mGunState = "firing"
		  self.mGunTimer = 0
		else
		  self.mGunState = "coolDown"
		  self.mGunTimer = mechGunCooldownTime
		  tech.playSound("mechGunCooldownSound")
		  self.forceWalkGun = false
		end		
	  elseif self.mGunState == "firing" and self.mGunTimer <= 0 then
	    -- If crouching, reduce fire cone, gun kick and recoil; if in the air, increase fire cone and kick (but reduce recoil due to weird physics)
		if self.isCrouching then
		  mechGunRecoilKick = mechGunRecoilKick * 0.5
		  -- mechGunFireCone = mechGunFireCone * 0.5
		  mechGunRecoilSpeed = mechGunRecoilSpeed * 0.85
		  mechGunRecoilPower = mechGunRecoilPower * 0.85
		elseif not mcontroller.onGround() then
		  mechGunRecoilKick = mechGunRecoilKick * 1.25
		  mechGunFireCone = mechGunFireCone * 1.5
		  mechGunRecoilSpeed = mechGunRecoilSpeed * 0.85
		  mechGunRecoilPower = mechGunRecoilPower * 0.5
		end
	    -- Fire shot
		local firePoint = vec_add(mcontroller.position(), mechChooseObject(tech.anchorPoint("frontGunFirePoint"), tech.anchorPoint("backGunFirePoint"), mechActiveSide, self.mechFlipped))
		local fireAngle = aimAngle - mechGunFireCone + math.random() * 2 * mechGunFireCone
		world.spawnProjectile(mechProjectile, firePoint, entity.id(), {math.cos(fireAngle), math.sin(fireAngle)}, false, mechProjectileConfig)
		energyCostAccumulated = energyCostAccumulated + energyCostPerBullet
	    self.mGunState = "fireCycle"
		self.mGunTimer = mechFireCycle
		tech.playSound("mechFireSound")
		mechAnimateActiveArm("frontFiring", "backFiring", "fire", mechActiveSide, self.mechFlipped)	
		-- Casing ejection
		local casingPoint = vec_add(mcontroller.position(), mechChooseObject(tech.anchorPoint("frontGunCasingPoint"), tech.anchorPoint("backGunCasingPoint"), mechActiveSide, self.mechFlipped))
		world.spawnProjectile(mechCasingProjectile, casingPoint, entity.id(), {math.random() - 0.5, 1}, false)
		-- Recoil
		self.mGunRecoilKick = self.mGunRecoilKick + mechGunRecoilKick
		local diagX = -math.cos(fireAngle)
		mcontroller.controlApproachXVelocity(mechGunRecoilSpeed * diagX, mechGunRecoilPower * math.abs(diagX), true)	
	  elseif self.mGunState == "fireCycle" and self.mGunTimer <= 0 then
	    if mechActiveSideButton and status.resource("energy") > energyCostPerBullet + 1 then
		  self.mGunState = "firing"
		  self.mGunTimer = 0
		else
		  self.mGunState = "coolDown"
		  self.mGunTimer = 0
		  tech.playSound("mechEndFireSound")
		  tech.playSound("mechGunCooldownSound")
		  self.forceWalkGun = false
		  self.mGunRecoilKick = 0
		end
	  elseif self.mGunState == "coolDown" and self.mGunTimer <= 0 then
		self.mGunState = "idle"
		self.mGunTimer = 0	
		self.mGunRecoilKick = 0
	  end
	  
	  -- Animation
	  local animAnglePauldron = {"gun-60Idle", "gun-30Idle", "gun+30Idle", "gun+60Idle", "gun+90Idle"}
	  mechAnimateActiveArm("frontArmPauldronMovement", nil, chooseAnimAngle(aimAngle, -mechAimLimit, mechAimLimit, animAnglePauldron, self.mechFlipped), mechActiveSide, self.mechFlipped)
	  if self.mGunState == "firing" then
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "idle", mechActiveSide, self.mechFlipped)
	    mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
	  elseif self.mGunState == "fireCycle" then
	    mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "fire", mechActiveSide, self.mechFlipped)
	    mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)	
	  elseif self.mGunState == "idle" or self.mGunState == "chargeUp" or self.mGunState == "coolDown" then
	    mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "idle", mechActiveSide, self.mechFlipped)
	    mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)		  	  
	  end
	end
	
    return 0 -- tech.consumeTechEnergy(energyCostPerSecond * args.dt + energyCostAccumulated)
  end  
  
  return 0
end
