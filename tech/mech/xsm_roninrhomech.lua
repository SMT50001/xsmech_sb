function init()
  self.active = false
  tech.setVisible(false)
  tech.rotateGroup("frontArm", 0, true)
  tech.rotateGroup("backArm", 0, true)
  tech.rotateGroup("frontArmPauldron", 0, true)
  tech.rotateGroup("frontHover", 0, true)
  tech.rotateGroup("backHover", 0, true)
  tech.rotateGroup("cannon", 0, true)
  self.holdingJump = false
  self.holdingUp = false
  self.holdingDown = false
  self.holdingLeft = false
  self.holdingRight = false
  self.holdingFire = false
  self.firePressed = false
  self.fireReleased = false
  self.holdingAltFire = false
  self.altFirePressed = false
  self.altFireReleased = false
  self.mechFlipped = false
  self.forceWalk = false
  self.lightTimer = 0
  
  -- Activate/Deactivate
  self.mechState = "off" -- Possible values "off" "turningOn" "on" "turningOff"
  self.mechStateTimer = 0
  
  -- Init landing variables
  self.fastDrop = false
  self.veryFastDrop = false
  self.veryVeryFastDrop = false
  self.movementSuppressed = false
  self.immobileTimer = 0
  
  -- Init jumping variables (short jump, dash, hover)
  self.isJumping = false
  self.hasJumped = false
  self.jumpPressed = false
  self.jumpReleased = false
  self.jumpTimer = 0
  self.jumpCoolDown = 0
  self.isDashing = false
  self.dashTimer = 0
  self.dashDirection = 0
  self.dashLastInput = 0
  self.dashTapLast = 0
  self.dashTapTimer = 0
  self.isHovering = false
  self.hoverTimer = 0
  self.backHopTimer = 0
  self.isSprinting = false
  
  -- Init weapon variables
  self.rightArmOccupied = false
  self.leftArmOccupied = false
  self.frontArmOccupied = false
  self.backArmOccupied = false
  self.torsoOccupied = false
  self.gunRotationSuppressed = false
  self.gunRotationOld = 0
  self.attackQueued = false
  self.attacking = false
  self.beamTimer = 0
  self.hGunTimer = 0
  self.fireTimer = 0
  self.hGunState = "idle"
  self.forceWalkGun = false
  self.mGunRecoilKick = 0
  self.parentAim = "right"
  
end

function uninit()
  if self.active then
    local mechTransformPositionChange = tech.parameter("mechTransformPositionChange")
    mcontroller.translate({-mechTransformPositionChange[1], -mechTransformPositionChange[2]})
    tech.setParentOffset({0, 0})
    self.active = false
    tech.setVisible(false)
    tech.setParentState()
    tech.setToolUsageSuppressed(false)
    mcontroller.controlFace(nil)
	
	-- Animation / Particles
	tech.setAnimationState("movement", "idle")
	tech.setAnimationState("jetFlame", "off")
	tech.setAnimationState("jetPack", "off")
	tech.setAnimationState("hoverFlame", "off")
	tech.setAnimationState("crotch", "turnOn")
	tech.setAnimationState("torso", "idle")
	tech.setAnimationState("backArmAltMovement", "off")
	tech.setAnimationState("frontArmAltMovement", "off")
	tech.setAnimationState("backArmMovement", "off")
	tech.setAnimationState("frontArmMovement", "off")
	tech.setAnimationState("frontArmPauldronMovement", "off")
	tech.setAnimationState("frontFiring", "off")
	tech.setAnimationState("backFiring", "off")
	tech.setAnimationState("frontLaser", "off")
	tech.setAnimationState("backLaser", "off")
	tech.setParticleEmitterActive("hoverFParticles", false)
	tech.setParticleEmitterActive("hoverParticles", false)
	tech.setParticleEmitterActive("hoverBParticles", false)
	tech.setParticleEmitterActive("skidParticles", false)
	tech.setParticleEmitterActive("veryHardLandingParticles", false)
	tech.setParticleEmitterActive("hardLandingParticles", false)
	tech.setParticleEmitterActive("mechSteam2", false)
	tech.setParticleEmitterActive("mechSteam", false)
	
  end
end

function input(args)
  -- Check if player is holding jump first
  if args.moves["jump"] then
    self.holdingJump = true
  elseif not args.moves["jump"] then
    self.holdingJump = false
  end
  
  -- Checks if jump was released
  if not self.jumpReleased then
    if self.holdingJump and not self.jumpPressed and not self.jumpReleased then
	  self.jumpPressed = true
    elseif not self.holdingJump and self.jumpPressed and not self.jumpReleased then
	  self.jumpReleased = true
	  self.jumpPressed = false
    end
  end
  
  -- Check if player is holding fire
  if args.moves["primaryFire"] then
    self.holdingFire = true
  elseif not args.moves["primaryFire"] then
    self.holdingFire = false
  end 
 
  -- Checks if fire was released
  if not self.fireReleased then
    if args.moves["primaryFire"] and not self.firePressed and not self.fireReleased then
	  self.firePressed = true
    elseif not args.moves["primaryFire"] and self.firePressed and not self.fireReleased then
	  self.fireReleased = true
	  self.firePressed = false
    end
  end
  
  -- Check if player is holding altFire
  if args.moves["altFire"] then
    self.holdingAltFire = true
  elseif not args.moves["altFire"] then
    self.holdingAltFire = false
  end 
 
  -- Checks if altFire was released
  if not self.altFireReleased then
    if args.moves["altFire"] and not self.altFirePressed and not self.altFireReleased then
	  self.altFirePressed = true
    elseif not args.moves["altFire"] and self.altFirePressed and not self.altFireReleased then
	  self.altFireReleased = true
	  self.altFirePressed = false
    end
  end
  
  -- Checks if player is holding other buttons
  if args.moves["up"] then
    self.holdingUp = true
  elseif not args.moves["up"] then
    self.holdingUp = false
  end
  
  if args.moves["down"] then
    self.holdingDown = true
  elseif not args.moves["down"] then
    self.holdingDown = false
  end
  
  if args.moves["left"] then
    self.holdingLeft = true
  elseif not args.moves["left"] then
    self.holdingLeft = false
  end
  
  if args.moves["right"] then
    self.holdingRight = true
  elseif not args.moves["right"] then
    self.holdingRight = false
  end

  -- Double tap input for boosting -- blatantly stolen from dash.lua. Thanks Chucklefish!
  if self.dashTimer <= 0 then
	  local maximumDoubleTapTime = tech.parameter("maximumDoubleTapTime")
	  local dashDuration = tech.parameter("dashDuration")

	  if self.dashTapTimer > 0 then
		self.dashTapTimer = self.dashTapTimer - args.dt
	  end

	  if args.moves["right"] then
		if self.dashLastInput ~= 1 then
		  if self.dashTapLast == 1 and self.dashTapTimer > 0 then
			self.dashTapLast = 0
			self.dashTapTimer = 0
			return "dashRight"
		  else
			self.dashTapLast = 1
			self.dashTapTimer = maximumDoubleTapTime
		  end
		end
		self.dashLastInput = 1
	  elseif args.moves["left"] then
		if self.dashLastInput ~= -1 then
		  if self.dashTapLast == -1 and self.dashTapTimer > 0 then
			self.dashTapLast = 0
			self.dashTapTimer = 0
			return "dashLeft"
		  else
			self.dashTapLast = -1
			self.dashTapTimer = maximumDoubleTapTime
		  end
		end
		self.dashLastInput = -1
	  else
		self.dashLastInput = 0
	  end
  end  
  
  if args.moves["special"] == 1 then
    if self.active then
      return "mechDeactivate"
    else
      return "mechActivate"
    end
  elseif args.moves["primaryFire"] then
    return "mechFire"
  end

  return nil
end

function randomProjectileLine(startPoint, endPoint, stepSize, projectile, chanceToSpawn)
  local dist = math.distance(startPoint, endPoint)
  local steps = math.floor(dist / stepSize)
  local normX = (endPoint[1] - startPoint[1]) / dist
  local normY = (endPoint[2] - startPoint[2]) / dist
  for i = 0, steps do
    local p1 = { normX * i * stepSize + startPoint[1], normY * i * stepSize + startPoint[2]}
	local p2 = { normX * (i + 1) * stepSize + startPoint[1], normY * (i + 1) * stepSize + startPoint[2]} 
	if math.random() <= chanceToSpawn then
	  world.spawnProjectile(projectile, math.midPoint(p1, p2), entity.id(), {normX, normY}, false)
	end
  end
  return endPoint
end

function mechSetArmOccupied(side, state)
  if side == "right" then
    self.rightArmOccupied = state
  elseif side == "left" then
    self.leftArmOccupied = state
  end
end

function mechCheckArmOccupied(side)
  if side == "right" then
    return self.rightArmOccupied
  elseif side == "left" then
    return self.leftArmOccupied
  elseif side == "front" then
    return self.frontArmOccupied
  elseif side == "back" then
    return self.backArmOccupied
  end
end

function mechAnimateActiveArm(frontState, backState, anim, side, flipState)
-- Animates a given arm (side = "left" or "right"). flipState is a global that keeps track of mech direction.
  if anim == nil then
    return 0
  end

  if side == "right" then
    if flipState then
	  if backState ~= nil then tech.setAnimationState(backState, anim) end
	else
	  if frontState ~= nil then tech.setAnimationState(frontState, anim) end
	end
  elseif side == "left" then
    if flipState then
	  if frontState ~= nil then tech.setAnimationState(frontState, anim) end
	else
	  if backState ~= nil then tech.setAnimationState(backState, anim) end
	end
  else
    -- Error message
  end
end

function mechAnimateArms(frontState, backState, frontAltState, backAltState, altSide, flipState, anim)
-- Animates front and back arms, if they are not busy doing something else. Used for walking animations etc.
-- Tracks asymmetry. Alt indicates the asymmetrical side.

  if not mechCheckArmOccupied(altSide) then
    mechAnimateActiveArm(frontAltState, backAltState, anim, altSide, flipState)
    mechAnimateActiveArm(frontState, backState, "off", altSide, flipState)
	-- world.logInfo("Animating %s", altSide)
	-- world.logInfo("with animation %s", anim)
  -- else
    -- world.logInfo("Failed to animate %s", altSide)
	-- world.logInfo("with animation %s", anim)
  end
  if not mechCheckArmOccupied(otherSide(altSide)) then
    mechAnimateActiveArm(frontState, backState, anim, otherSide(altSide), flipState)
    mechAnimateActiveArm(frontAltState, backAltState, "off", otherSide(altSide), flipState)
	-- world.logInfo("Animating %s" , otherSide(altSide))
	-- world.logInfo("with animation %s", anim)
  --else
   -- world.logInfo("Failed to animate %s", otherSide(altSide))
   -- world.logInfo("with animation %s", anim)
  end
end

function mechAnimateTorso(animState, anim)
  if not self.torsoOccupied then
    tech.setAnimationState(animState, anim)
  end
end

function mechAnimatePauldron(animState, anim)
  if not mechCheckArmOccupied("front") then
    tech.setAnimationState(animState, anim)
  end
end

function mechChooseObject(frontObject, backObject, side, flipState)
  if side == "right" then
    if flipState then
	  return backObject
	else
	  return frontObject
	end
  elseif side == "left" then
    if flipState then
	  return frontObject
	else
	  return backObject
	end
  elseif side == nil then
    if flipState then
	  return backObject
	else
	  return frontObject
	end
  end
end

function releaseInput(side)
  if side == "right" then
    self.altFireReleased = false
  elseif side == "left" then
    self.fireReleased = false
  end
end

-- Chooses an animation from animList based on the given angle between angleMin and angleMax
function chooseAnimAngle(angle, angleMin, angleMax, animList, flip)
  if #animList == 0 then
    return nil
  end
  local angleStep = (angleMax - angleMin) / #animList
  if flip then
    if angle > 0 then
      angle = math.pi - angle
	else
	  angle = -math.pi - angle
	end
  end
  if angle > angleMax then
    return animList[#animList]
  elseif angle <= angleMin then
    return animList[1]
  end
  for i, v in ipairs(animList) do
	if angle > (i-1) * angleStep + angleMin and angle <= i * angleStep + angleMin then
	  return animList[i]
	end
  end
  return nil
end

function spawnVariableDamageBeam(startPoint, endPoint, beamWidth, damageProjectile, damageUpper, damageLower, range)
  local queryStart = startPoint
  local queryEnd = endPoint
  if startPoint[1] > endPoint[1] then
	queryStart = {endPoint[1], queryStart[2]}
	queryEnd = {startPoint[1], queryEnd[2]}
  end
  if startPoint[2] > endPoint[2] then
	queryStart = {queryStart[1], endPoint[2]}
	queryEnd = {queryEnd[1], startPoint[2]}
  end  
  local entityTable = world.entityQuery(queryStart, queryEnd)
  local enemiesHit = 0
  local damageScalar = (damageUpper - damageLower)/range
  for _, v in ipairs(entityTable) do
	if world.entityType(v) == "player" or world.entityType(v) == "npc" or world.entityType(v) == "monster" then
	  local pos = world.entityPosition(v)
	  local distdX = startPoint[1] - endPoint[1]
	  local distdY = startPoint[2] - endPoint[2]
	  local d = math.abs( distdY * pos[1] - distdX * pos[2] + startPoint[1] * endPoint[2] - startPoint[2] * endPoint[1] ) / math.sqrt( (distdX)^2 + (distdY)^2 )
	  if d <= beamWidth / 2 then
	    local damage = math.min( (range - math.distance( world.entityPosition(v), startPoint)) * damageScalar, damageUpper)
		world.spawnProjectile(damageProjectile, pos, entity.id(), {0, 0}, false, {power = damage})
		enemiesHit = enemiesHit + 1
	  end
	end
  end
  return enemiesHit
end

function update(args)
  -- Energy usage
  local energyCostPerSecond = tech.parameter("energyCostPerSecond")
  local energyCostPerSecondRunning = tech.parameter("energyCostPerSecondRunning")
  local energyCostPerBackHop = tech.parameter("energyCostPerBackHop")
  local energyCostPerSecondHover = tech.parameter("energyCostPerSecondHover")
  local energyCostPerHover = tech.parameter("energyCostPerHover")
  local energyCostPerBeamFire = tech.parameter("energyCostPerBeamFire")
  local energyCostPerAltShot = tech.parameter("energyCostPerAltShot")
  local energyCostAccumulated = 0
  
  -- Movement / misc
  local mechCustomMovementParameters = tech.parameter("mechCustomMovementParameters")
  local mechCustomTurningOnOffMovementParameters = copy(tech.parameter("mechCustomMovementParameters"))
  mechCustomTurningOnOffMovementParameters.runSpeed = 0.0
  mechCustomTurningOnOffMovementParameters.walkSpeed = 0.0
  local mechTransformPositionChange = tech.parameter("mechTransformPositionChange")
  local parentOffset = tech.parameter("parentOffset")
  local mechCollisionTest = tech.parameter("mechCollisionTest")
  local mechBackwardSpeed = tech.parameter("mechBackwardSpeed")
  local mechWalkSlowSpeed = tech.parameter("mechWalkSlowSpeed")
  local mechSkiddingFriction = tech.parameter("mechSkiddingFriction")
  local mechBoostAirForce = tech.parameter("mechBoostAirForce")
  local mechLandingProjectile = tech.parameter("mechLandingProjectile")
  local mechLandingLocation = tech.parameter("mechLandingLocation")
  local mechStartupTime = tech.parameter("mechStartupTime")
  local mechShutdownTime = tech.parameter("mechShutdownTime")
  
  -- Jump, Dash, Boost
  local jumpDuration = tech.parameter("jumpDuration")
  local jumpSpeed = tech.parameter("jumpSpeed")
  local jumpControlForce = tech.parameter("jumpControlForce")
  local dashDuration = tech.parameter("dashDuration")
  local dashSpeed = tech.parameter("dashSpeed")
  local dashControlForce = tech.parameter("dashControlForce")
  local dashAngle = tech.parameter("dashAngle") * math.pi / 180
  local backHopDuration = tech.parameter("backHopDuration")
  local backHopSpeed = tech.parameter("backHopSpeed")
  local backHopControlForce = tech.parameter("backHopControlForce")
  local backHopAngle = tech.parameter("backHopAngle") * math.pi / 180
  local backHopCooldown = tech.parameter("backHopCooldown")
  local hoverSpeed = tech.parameter("hoverSpeed")
  local hoverControlForce = tech.parameter("hoverControlForce")
  
  -- Equipment
  local mechLeftEquip = tech.parameter("mechLeftEquip")
  local mechLeftAnimation = tech.parameter("mechLeftAnimation")
  local mechRightEquip = tech.parameter("mechRightEquip")
  local mechRightAnimation = tech.parameter("mechRightAnimation")
  local mechEquip = {mechLeftEquip, mechRightEquip}
  
  -- Torso cannon
  local mechCannonAimLimit = tech.parameter("mechCannonAimLimit") * math.pi / 180
  local mechCannonFireCone = tech.parameter("mechCannonFireCone") * math.pi / 180
  local mechCannonFireCycle = tech.parameter("mechCannonFireCycle")
  local mechCannonProjectile = tech.parameter("mechCannonProjectile")
  local mechCannonProjectileConfig = tech.parameter("mechCannonProjectileConfig")
  
  -- Heat beam
  local mechGunReadyTime = tech.parameter("mechGunReadyTime")
  local mechGunHolsterTime = tech.parameter("mechGunHolsterTime")
  local mechGunIdleTime = tech.parameter("mechGunIdleTime")
  local mechGunFireTime = tech.parameter("mechGunFireTime")
  local mechGunCoolingTime = tech.parameter("mechGunCoolingTime")
  local mechGunSuppAngularVel = tech.parameter("mechGunSuppAngularVel") * math.pi / 180
  local mechGunLightProjectile = tech.parameter("mechGunLightProjectile")
  local mechGunBeamMaxRange = tech.parameter("mechGunBeamMaxRange")
  local mechGunBeamStep = tech.parameter("mechGunBeamStep")
  local mechGunBeamUpdateTime = tech.parameter("mechGunBeamUpdateTime")
  local mechGunBeamEndProjectile = tech.parameter("mechGunBeamEndProjectile")
  local mechGunBeamHitProjectile = tech.parameter("mechGunBeamHitProjectile")
  local mechGunBeamSmokeProkectile = tech.parameter("mechGunBeamSmokeProkectile")
  local mechGunBeamWidth = tech.parameter("mechGunBeamWidth")
  local mechGunAimLimit = tech.parameter("mechGunAimLimit") * math.pi / 180
  local mechGunRotationRadius = tech.parameter("mechGunRotationRadius")
  local mechGunBeamUpperDamage = tech.parameter("mechGunBeamUpperDamage")
  local mechGunBeamLowerDamage = tech.parameter("mechGunBeamLowerDamage")

  local mechGunRecoilSpeed = tech.parameter("mechGunRecoilSpeed")
  local mechGunRecoilPower = tech.parameter("mechGunRecoilPower")
  local mechGunRecoilKick = tech.parameter("mechGunRecoilKick") * math.pi / 180
  
  if not self.active and args.actions["mechActivate"] and self.mechState == "off" and mcontroller.onGround() then
    mechCollisionTest[1] = mechCollisionTest[1] + mcontroller.position()[1]
    mechCollisionTest[2] = mechCollisionTest[2] + mcontroller.position()[2]
    mechCollisionTest[3] = mechCollisionTest[3] + mcontroller.position()[1]
    mechCollisionTest[4] = mechCollisionTest[4] + mcontroller.position()[2]
    if (not world.rectCollision(mechCollisionTest)) then
      -- tech.burstParticleEmitter("mechActivateParticles")
      mcontroller.translate(mechTransformPositionChange)
      tech.setVisible(true)
	  tech.setAnimationState("movement", "idle")
      tech.setParentState("sit")
      tech.setToolUsageSuppressed(true)

	  self.mechState = "turningOn"
	  self.mechStateTimer = mechStartupTime
	  self.active = true
	  tech.playSound("mechStartupSound")
	  
	  local diff = world.distance(tech.aimPosition(), mcontroller.position())
      local aimAngle = math.atan2(diff[2], diff[1])
      self.mechFlipped = aimAngle > math.pi / 2 or aimAngle < -math.pi / 2
    else
      -- Make some kind of error noise
    end	  
  elseif self.mechState == "turningOn" and self.mechStateTimer <= 0 then
    self.mechState = "on"
	self.mechStateTimer = 0
  elseif (self.active and (args.actions["mechDeactivate"] and self.mechState == "on" and mcontroller.onGround()) or (energyCostPerSecond * args.dt > status.resource("energy")) and self.mechState == "on") then
	self.mechState = "turningOff"
	self.mechStateTimer = mechShutdownTime
	tech.playSound("mechShutdownSound")
  elseif self.mechState == "turningOff" and self.mechStateTimer <= 0 then 
	-- tech.burstParticleEmitter("mechDeactivateParticles")
	-- world.spawnProjectile("xsm_rhomechwarpout", mcontroller.position(), entity.id(), {0, 0}, false)
	
	tech.setVisible(false)
    tech.setParentState()  
    mcontroller.translate({-mechTransformPositionChange[1], -mechTransformPositionChange[2]})
    tech.setToolUsageSuppressed(false)
    tech.setParentOffset({0, 0})
	
    -- Activate/Deactivate
    self.mechState = "off"
    self.mechStateTimer = 0
	
    self.firePressed = false
    self.fireReleased = false
    self.altFirePressed = false
    self.altFireReleased = false

	-- Landing
	self.fastDrop = false
	self.veryFastDrop = false
	self.veryVeryFastDrop = false
	self.movementSuppressed = false
	self.immobileTimer = 0
	
	-- Jumping (short jump, dash, boost)
	self.isJumping = false
	self.hasJumped = false
	self.jumpPressed = false
	self.jumpReleased = false
	self.jumpTimer = 0
	self.jumpCoolDown = 0
	self.isDashing = false
	self.dashTimer = 0
    self.dashDirection = 0
    self.dashLastInput = 0
    self.dashTapLast = 0
    self.dashTapTimer = 0
	self.isHovering = false
	self.hoverTimer = 0
	self.backHopTimer = 0
	self.isSprinting = false
	
	-- Animation / Particles
	tech.setAnimationState("movement", "idle")
	tech.setAnimationState("jetFlame", "off")
	tech.setAnimationState("jetPack", "off")
	tech.setAnimationState("hoverFlame", "off")
	tech.setAnimationState("crotch", "turnOn")
	tech.setAnimationState("torso", "idle")
	tech.setAnimationState("backArmAltMovement", "off")
	tech.setAnimationState("frontArmAltMovement", "off")
	tech.setAnimationState("backArmMovement", "off")
	tech.setAnimationState("frontArmMovement", "off")
	tech.setAnimationState("frontArmPauldronMovement", "off")
	tech.setAnimationState("frontFiring", "off")
	tech.setAnimationState("backFiring", "off")
	tech.setAnimationState("frontLaser", "off")
	tech.setAnimationState("backLaser", "off")
	tech.setParticleEmitterActive("hoverFParticles", false)
	tech.setParticleEmitterActive("hoverParticles", false)
	tech.setParticleEmitterActive("hoverBParticles", false)
	tech.setParticleEmitterActive("skidParticles", false)
	tech.setParticleEmitterActive("veryHardLandingParticles", false)
	tech.setParticleEmitterActive("hardLandingParticles", false)
	tech.setParticleEmitterActive("mechSteam2", false)
	tech.setParticleEmitterActive("mechSteam", false)
	
	-- Weapons
	self.forceWalk = false
	self.attackQueued = false
	self.attacking = false
    self.rightArmOccupied = false
    self.leftArmOccupied = false
    self.frontArmOccupied = false
    self.backArmOccupied = false
	self.torsoOccupied = false
	self.gunRotationSuppressed = false
	self.gunRotationOld = 0
	self.beamTimer = 0
	self.hGunTimer = 0
	self.fireTimer = 0
	self.hGunState = "idle"
	self.forceWalkGun = false
	self.mGunRecoilKick = 0
	
    self.active = false
  end

  mcontroller.controlFace(nil)
  
  if self.mechStateTimer > 0 then
	self.mechStateTimer = self.mechStateTimer - args.dt
  end

  local diff = world.distance(tech.aimPosition(), mcontroller.position())
  local aimAngle = math.atan2(diff[2], diff[1])
  local cannonAimAngle = aimAngle
  local flip = aimAngle > math.pi / 2 or aimAngle < -math.pi / 2 

  -- Basic animation
  if self.active then
  
	mcontroller.controlParameters(mechCustomMovementParameters)
    
	-- Gun angle correction
	if table.exists("heatBeam", mechEquip) and self.mechState == "on" then
	  local gunSide = nil
	  local gunRotationPosition = {}
	  -- self.parentAim indicates which arm is the primary aiming arm. It is also used (somewhat unintuitively) as a proxy to determine whether rotation should be applied.
	  if mechLeftEquip == "heatBeam" and self.hGunState ~= "idle" and self.hGunState ~= "holstering" then
		gunSide = "left"
		self.parentAim = "left"
	  elseif mechRightEquip == "heatBeam" and self.hGunState ~= "idle" and self.hGunState ~= "holstering" then
		gunSide = "right"
		self.parentAim = "right"
	  else
	    gunSide = "right"
	    self.parentAim = nil
		tech.rotateGroup("frontArmPauldron", 0)
	  end
	  gunRotationPosition = vec_add(mcontroller.position(), mechChooseObject( tech.anchorPoint("frontGunRotationPoint"), tech.anchorPoint("backGunRotationPoint"), gunSide, self.mechFlipped))
	  local radius = mechGunRotationRadius
	  local range = math.distance(tech.aimPosition(), gunRotationPosition)
	  local correction = math.asin( radius / range )
	  local diff2 = world.distance(tech.aimPosition(), gunRotationPosition)
	  local refAngle = math.atan2(diff2[2], diff2[1])
	  
	  -- Protection against gun wildly swinging at close range
	  if range <= 2 * radius then
	    correction = 0
	  end
	  
	  -- Apply correction
	  if self.mechFlipped then
	    aimAngle = refAngle - correction - self.mGunRecoilKick
	  else
	    aimAngle = refAngle + correction + self.mGunRecoilKick
	  end
	end
	
	-- Reduce rotation speed if rotation is suppressed
	if self.gunRotationSuppressed then
	  -- world.logInfo("aimAngle is: %s", aimAngle)
	  -- world.logInfo("self.gunRotationOld is: %s", self.gunRotationOld)
	  if self.mechFlipped then
	    if (aimAngle < 0 and self.gunRotationOld < 0) or (aimAngle > 0 and self.gunRotationOld > 0) then
	  	  if aimAngle > self.gunRotationOld then
		    aimAngle = self.gunRotationOld + mechGunSuppAngularVel * args.dt
	      elseif aimAngle < self.gunRotationOld then
	        aimAngle = self.gunRotationOld - mechGunSuppAngularVel * args.dt
  	      end
		else
	  	  if aimAngle > self.gunRotationOld then
		    aimAngle = self.gunRotationOld - mechGunSuppAngularVel * args.dt
	      elseif aimAngle < self.gunRotationOld then
	        aimAngle = self.gunRotationOld + mechGunSuppAngularVel * args.dt
  	      end		  
	    end
	  else
	    if aimAngle > self.gunRotationOld then
		  aimAngle = self.gunRotationOld + mechGunSuppAngularVel * args.dt
	    elseif aimAngle < self.gunRotationOld then
	      aimAngle = self.gunRotationOld - mechGunSuppAngularVel * args.dt
  	    end
	  end
	end  

	-- Flip mech, gun angles, etc.
	if (not self.isDashing) and (not self.attacking) and (self.hGunState == "limbo" or self.hGunState == "idle") and self.mechState == "on" then
		if flip then  
		  self.mechFlipped = true
		else
		  self.mechFlipped = false
		end
	end	
	if self.mechFlipped then
	  tech.setFlipped(true)  
	  if self.rightArmOccupied then
		self.backArmOccupied = true
	  else
		self.backArmOccupied = false
	  end
	  if self.leftArmOccupied then
		self.frontArmOccupied = true
	  else
		self.frontArmOccupied = false
	  end				  
	  local nudge = tech.appliedOffset()
	  tech.setParentOffset({-parentOffset[1] - nudge[1], parentOffset[2] + nudge[2]})
	  mcontroller.controlFace(-1)	
	  -- Limits of aimAngle
      if aimAngle > 0 then
        aimAngle = math.max(aimAngle, math.pi - mechGunAimLimit)
      else
        aimAngle = math.min(aimAngle, -math.pi + mechGunAimLimit)
      end
	  if cannonAimAngle > 0 then
	    cannonAimAngle = math.max(cannonAimAngle, math.pi - mechCannonAimLimit)
	  else
	    cannonAimAngle = math.min(cannonAimAngle, -math.pi + mechCannonAimLimit)
	  end
	  -- Rotate arms
	  if self.mechState == "on" and self.parentAim ~= nil then
	    if self.parentAim == "right" then
		  tech.rotateGroup("backArm", math.pi - aimAngle)
		else
	      tech.rotateGroup("frontArm", math.pi - aimAngle)	
		end
	  else
	    tech.rotateGroup("frontArm", 0)
		tech.rotateGroup("backArm", 0)
	  end
	  if self.mechState == "on" then
	    tech.rotateGroup("cannon", math.pi - cannonAimAngle)
	  else
	    tech.rotateGroup("cannon", 0)
	  end
	else
	  tech.setFlipped(false)
	  if self.rightArmOccupied then
		self.frontArmOccupied = true
	  else
		self.frontArmOccupied = false
	  end
	  if self.leftArmOccupied then
		self.backArmOccupied = true
	  else
		self.backArmOccupied = false
	  end	
	  local nudge = tech.appliedOffset()
	  tech.setParentOffset({parentOffset[1] + nudge[1], parentOffset[2] + nudge[2]})
	  mcontroller.controlFace(1)	
	  -- Limits of aimAngle
      if aimAngle > 0 then
        aimAngle = math.min(aimAngle, mechGunAimLimit)
      else
        aimAngle = math.max(aimAngle, -mechGunAimLimit)
      end	
      if cannonAimAngle > 0 then
        cannonAimAngle = math.min(cannonAimAngle, mechCannonAimLimit)
      else
        cannonAimAngle = math.max(cannonAimAngle, -mechCannonAimLimit)
      end		  
	  -- Rotate arms
	  if self.mechState == "on" and self.parentAim ~= nil then
	    if self.parentAim == "right" then
	      tech.rotateGroup("frontArm", aimAngle)
		else
		  tech.rotateGroup("backArm", aimAngle)		
		end
	  else
	    tech.rotateGroup("frontArm", 0)
		tech.rotateGroup("backArm", 0)
	  end
	  if self.mechState == "on" then
	    tech.rotateGroup("cannon", cannonAimAngle)
	  else
	    tech.rotateGroup("cannon", 0)
	  end
	end 
	self.gunRotationOld = aimAngle
  end
  
  -- Startup, shutdown, and active
  if self.mechState == "turningOn" then
	mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)
	tech.setAnimationState("movement", "stand")
	tech.setAnimationState("crotch", "turnOn")
	mechAnimateTorso("torso", "turnOn")
	mechAnimatePauldron("frontArmPauldronMovement", "idle")
	mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "idle")
  elseif self.mechState == "turningOff" then
	mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)
	tech.setAnimationState("movement", "sit")
	mechAnimateTorso("torso", "turnOff")
	tech.setAnimationState("crotch", "turnOff")
	mechAnimatePauldron("frontArmPauldronMovement", "idle")
	mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "idle")
	tech.setAnimationState("frontFiring", "off")
	tech.setAnimationState("backFiring", "off")
	tech.setAnimationState("frontLaser", "off")
	tech.setAnimationState("backLaser", "off")
  elseif self.active and self.mechState == "on" then
	
	-- Misc. Animation-related checks
	--mechSetArmOccupied ("left", true) TEMP
	--mechSetArmOccupied ("right", true) TEMP
	
	-- State checks, animation
    if not mcontroller.onGround() then
      if mcontroller.velocity()[2] > 0 and (not self.isHovering) then
	    -- Jump animation only
        tech.setAnimationState("movement", "jump")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "jump")
		mechAnimatePauldron("frontArmPauldronMovement", "jump")
		mechAnimateTorso("torso", "idle")
		tech.setAnimationState("crotch", "idle")
		self.fastDrop = false
	    self.veryFastDrop = false
      elseif not self.movementSuppressed and (not self.isHovering) then
	    -- Falling animation. Hard landings and very hard landings suppress movement temporarily.
		if (not self.hasJumped) and (not self.isHovering) then
	      self.jumpReleased = true
		  self.hasJumped = true
		end
        tech.setAnimationState("movement", "fall")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "fall")
		mechAnimatePauldron("frontArmPauldronMovement", "fall")
		mechAnimateTorso("torso", "idle")
		tech.setAnimationState("crotch", "idle")
		if (mcontroller.velocity()[2] < -30 and mcontroller.velocity()[2] > -52) and math.abs(mcontroller.velocity()[1]) < 30 then
		  self.fastDrop = true
		elseif mcontroller.velocity()[2] <= -52 and mcontroller.velocity()[2] > -68 then
		  self.fastDrop = false
		  self.veryFastDrop = true
		elseif mcontroller.velocity()[2] <= -68 then
		  self.fastDrop = false
		  self.veryFastDrop = false
		  self.veryVeryFastDrop = true
		end	
      end
	  if self.holdingJump and (not self.isHovering) and self.jumpReleased and (not self.isDashing) and (not self.holdingDown) and status.resource("energy") > (energyCostPerHover + energyCostPerSecondHover * 0.4) then
	    -- Initiate boosting
		self.jumpReleased = false	    
		self.isHovering = true
		tech.playSound("hoverInitiateSound")
		energyCostAccumulated = energyCostAccumulated + energyCostPerHover
	  end
	else
	  self.jumpReleased = false
	  self.hasJumped = false
		if self.fastDrop and not self.movementSuppressed then
		  -- Hard landing
		  self.immobileTimer = 0.15 -- TEMP
		  self.movementSuppressed = true	  
		  tech.burstParticleEmitter("hardLandingParticles")
		elseif self.veryFastDrop and not self.movementSuppressed then
		  -- Very hard landing	  
		  self.immobileTimer = 0.66 -- TEMP
		  self.movementSuppressed = true
		  tech.burstParticleEmitter("veryHardLandingParticles")
		elseif self.veryVeryFastDrop and not self.movementSuppressed then
		  -- Very, Very hard landing
		  self.immobileTimer = 0.78 -- TEMP
		  self.movementSuppressed = true
		  tech.burstParticleEmitter("veryHardLandingParticles")	
		  local particlePosition = {mcontroller.position()[1] + mechLandingLocation[1], mcontroller.position()[2] + mechLandingLocation[2]}
		  world.spawnProjectile(mechLandingProjectile, particlePosition, entity.id(), {0, 0}, false)
		elseif self.holdingJump and not self.isJumping and not self.movementSuppressed and not self.holdingDown then
		  -- Activate hop (jumping)
		  self.isJumping = true
		  self.jumpTimer = jumpDuration
		  tech.playSound("jumpSound")
		  self.hasJumped = true
		elseif (mcontroller.walking() or mcontroller.running()) and not self.movementSuppressed then
		  -- walk/run/backWalk/walkSlow/backWalkSlow animations
		  self.fastDrop = false
		  self.veryFastDrop = false
		  local mechCustomAnimationMovementParameters = copy(tech.parameter("mechCustomMovementParameters"))
		  if self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1 then
		    if self.forceWalk then
			  tech.setAnimationState("movement", "backWalkSlow")
			  mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "backWalkSlow")
			  mechAnimatePauldron("frontArmPauldronMovement", "backWalkSlow")
			  mechAnimateTorso("torso", "backWalkSlow")
			  tech.setAnimationState("crotch", "backWalkSlow")
			  mechCustomAnimationMovementParameters.runSpeed = mechWalkSlowSpeed
			  mechCustomAnimationMovementParameters.walkSpeed = mechWalkSlowSpeed
			  mcontroller.controlParameters(mechCustomAnimationMovementParameters)	
			else  
			  tech.setAnimationState("movement", "backWalk")
			  mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "backWalk")
			  mechAnimatePauldron("frontArmPauldronMovement", "backWalk")
			  mechAnimateTorso("torso", "backWalk")
			  tech.setAnimationState("crotch", "backWalk")
			  mechCustomAnimationMovementParameters.runSpeed = mechBackwardSpeed
			  mechCustomAnimationMovementParameters.walkSpeed = mechBackwardSpeed
			  mcontroller.controlParameters(mechCustomAnimationMovementParameters)
			end
		  else
		    if self.forceWalk then
			  tech.setAnimationState("movement", "walkSlow")
			  mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "walkSlow")
			  mechAnimatePauldron("frontArmPauldronMovement", "walkSlow")
			  mechAnimateTorso("torso", "walkSlow")
			  tech.setAnimationState("crotch", "walkSlow")
			  mechCustomAnimationMovementParameters.runSpeed = mechWalkSlowSpeed
			  mechCustomAnimationMovementParameters.walkSpeed = mechWalkSlowSpeed
			  mcontroller.controlParameters(mechCustomAnimationMovementParameters)		
			else
			  if mcontroller.running() then
			    tech.setAnimationState("movement", "walk")
				mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "walk")
				mechAnimatePauldron("frontArmPauldronMovement", "walk")
				mechAnimateTorso("torso", "walk")
				tech.setAnimationState("crotch", "walk")
			  elseif mcontroller.walking() then
			    tech.setAnimationState("movement", "run")
				mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "run")
				mechAnimatePauldron("frontArmPauldronMovement", "run")
				mechAnimateTorso("torso", "run")
				tech.setAnimationState("crotch", "run")
				tech.burstParticleEmitter("mechSteam2", true)
				energyCostAccumulated = energyCostAccumulated + energyCostPerSecondRunning * args.dt
			  end
			end
		  end
		elseif not self.movementSuppressed then
		  -- Idle
		  self.fastDrop = false
		  self.veryFastDrop = false
			-- Crouching
		  if self.holdingDown then
			tech.setAnimationState("movement", "sit")
			mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "idle")
			mechAnimatePauldron("frontArmPauldronMovement", "idle")
			mechAnimateTorso("torso", "idle")
			tech.setAnimationState("crotch", "idle")
		  else
		    tech.setAnimationState("movement", "idle")
			mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "idle")
			mechAnimatePauldron("frontArmPauldronMovement", "idle")
			mechAnimateTorso("torso", "idle")
			tech.setAnimationState("crotch", "idle")
		  end
		end
	end
	
	-- Temp fall protection
	if mcontroller.velocity()[2] < -70 then
		mcontroller.controlApproachYVelocity(-70, 6000, false)
	end

	-- Side sprinting 
	-- if self.isSprinting and mcontroller.walking() and mcontroller.onGround() then
	  -- tech.setAnimationState("movement", "sit")
	  -- tech.setAnimationState("jetPack", "boost3")
	  -- tech.setAnimationState("jetFlame", "dash")
	  -- mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "jump")
	  -- mechAnimatePauldron("frontArmPauldronMovement", "jump")
	  -- mechAnimateTorso("torso", "idle")
	  -- tech.setAnimationState("crotch", "idle")
	  -- tech.burstParticleEmitter("mechSteam2", true)
	  -- energyCostAccumulated = energyCostAccumulated + energyCostPerSecondRunning * args.dt
	  -- if self.mechFlipped then
	    -- mcontroller.controlApproachXVelocity(-dashSpeed, hoverControlForce, true)
	  -- else
	    -- mcontroller.controlApproachXVelocity(dashSpeed, hoverControlForce, true)
	  -- end
	-- elseif self.isSprinting and (not mcontroller.walking() or not mcontroller.onGround()) then
	  -- self.isSprinting = false
	  -- tech.setAnimationState("jetPack", "turnoff3")
	  -- tech.setAnimationState("jetFlame", "off")
	-- end	
	
	-- Jumping physics
	if self.isJumping and self.jumpTimer > 0 and self.holdingJump and not self.movementSuppressed then
	  mcontroller.controlApproachYVelocity(jumpSpeed, jumpControlForce, true)
	  self.jumpTimer = self.jumpTimer - args.dt
	elseif self.isJumping then
	  self.isJumping = false
	  self.jumpTimer = 0
	end
	
	-- Dashing physics
	if args.actions["dashRight"] and self.dashTimer <= 0 and not self.movementSuppressed then
	  if (self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1) and mcontroller.onGround() and status.resource("energy") > energyCostPerBackHop then
	    if self.backHopTimer <= 0 then
		  self.dashTimer = backHopDuration
	      self.dashDirection = 1	  
		  energyCostAccumulated = energyCostAccumulated + energyCostPerBackHop
		  tech.playSound("backHopSound")
		  self.backHopTimer = backHopCooldown
		end
	  elseif mcontroller.onGround() and status.resource("energy") > energyCostPerBackHop then
	    if self.backHopTimer <= 0 then
		  self.dashTimer = backHopDuration
	      self.dashDirection = 2	  
		  energyCostAccumulated = energyCostAccumulated + energyCostPerBackHop
		  tech.playSound("backHopSound")
		  self.backHopTimer = backHopCooldown
		end
	  -- elseif not (self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1) and status.resource("energy") > energyCostPerDash then
	    -- self.dashTimer = dashDuration
	    -- self.dashDirection = 3
		-- energyCostAccumulated = energyCostAccumulated + energyCostPerDash
		-- tech.playSound("dashSound")
	  end
	  self.isDashing = true
	  self.jumpReleased = true
	elseif args.actions["dashLeft"] and self.dashTimer <= 0 and not self.movementSuppressed then
	  if (self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1) and mcontroller.onGround() and status.resource("energy") > energyCostPerBackHop then
	    if self.backHopTimer <= 0 then
		  self.dashTimer = backHopDuration
	      self.dashDirection = -1	 
		  energyCostAccumulated = energyCostAccumulated + energyCostPerBackHop
		  tech.playSound("backHopSound")
		  self.backHopTimer = backHopCooldown
		end
	  elseif mcontroller.onGround() and status.resource("energy") > energyCostPerBackHop then  
	    if self.backHopTimer <= 0 then
		  self.dashTimer = backHopDuration
	      self.dashDirection = -2	 
		  energyCostAccumulated = energyCostAccumulated + energyCostPerBackHop
		  tech.playSound("backHopSound")
		  self.backHopTimer = backHopCooldown
		end
	  -- elseif not (self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1) and status.resource("energy") > energyCostPerDash then
	    -- self.dashTimer = dashDuration
	    -- self.dashDirection = -3
		-- energyCostAccumulated = energyCostAccumulated + energyCostPerDash 
		-- tech.playSound("dashSound")
	  end
	  self.isDashing = true
	  self.jumpReleased = true
	end
	if self.dashTimer > 0 then
	  -- Backhop
	  if self.dashDirection == 1 then
		local diagX = math.cos(backHopAngle)
		local diagY = math.sin(backHopAngle)
		mcontroller.controlApproachVelocity({backHopSpeed * diagX, backHopSpeed * diagY}, backHopControlForce, true, true)	
		-- Backhop right animation + particles
	  elseif self.dashDirection == -1 then
		local diagX = -math.cos(backHopAngle)
		local diagY = math.sin(backHopAngle)
		mcontroller.controlApproachVelocity({backHopSpeed * diagX, backHopSpeed * diagY}, backHopControlForce, true, true)	
		-- Backhop left animation + particles  

	  -- Fronthop
	  elseif self.dashDirection == 2 then
		local diagX = math.cos(backHopAngle*1.5)
		local diagY = math.sin(backHopAngle*1.5)
		mcontroller.controlApproachVelocity({backHopSpeed * diagX, backHopSpeed * diagY}, backHopControlForce, true, true)	
	  elseif self.dashDirection == -2 then
		local diagX = -math.cos(backHopAngle*1.5)
		local diagY = math.sin(backHopAngle*1.5)
		mcontroller.controlApproachVelocity({backHopSpeed * diagX, backHopSpeed * diagY}, backHopControlForce, true, true)	
		
	  -- Full Power Dash (in Midair)
	  elseif self.dashDirection == 3 then
		mcontroller.controlApproachXVelocity(dashSpeed, dashControlForce, true)
		tech.setAnimationState("jetPack", "boost3")
		tech.setAnimationState("jetFlame", "dash")
	  elseif self.dashDirection == -3 then
		mcontroller.controlApproachXVelocity(-dashSpeed, dashControlForce, true)	
		tech.setAnimationState("jetPack", "boost3")
		tech.setAnimationState("jetFlame", "dash") 
	  end  
	  self.dashTimer = self.dashTimer - args.dt	  
	elseif self.isDashing then
	  self.isDashing = false
	  self.dashTimer = 0
	  tech.setAnimationState("jetPack", "turnoff3")
	  tech.setAnimationState("jetFlame", "off")
	  self.jumpReleased = true
	end
	
	-- Hovering physics
	if self.isHovering and (status.resource("energy") > ( energyCostPerSecondHover * 0.4 )) and (not mcontroller.onGround()) and not (self.jumpReleased and self.holdingJump) then
	  local actualHoverSpeed
	  if self.holdingDown and not self.holdingUp then
	    mcontroller.controlApproachYVelocity(hoverSpeed-10, hoverControlForce, false)
	  elseif self.holdingUp and not self.holdingDown then
	    mcontroller.controlApproachYVelocity(hoverSpeed+10, hoverControlForce, false)
	  else
	    mcontroller.controlApproachYVelocity(hoverSpeed, hoverControlForce, true)
	  end
	  --[[
	  local u = (math.random() * 2) - 1
	  local v = (math.random() * 2) - 1
	  mcontroller.controlApproachYVelocity(u * 10, 500, true)
	  mcontroller.controlApproachXVelocity(v * 4, 100, true)
	  --]]
	  local mechCustomHoveringMovementParameters = copy(tech.parameter("mechCustomMovementParameters"))
	  mechCustomHoveringMovementParameters.airForce = mechBoostAirForce
	  mcontroller.controlParameters(mechCustomHoveringMovementParameters)
	  energyCostAccumulated = energyCostAccumulated + energyCostPerSecondHover * args.dt
	  self.hoverTimer = self.hoverTimer + args.dt
	  tech.setAnimationState("hoverFlame", "on")
	  if (self.mechFlipped and self.holdingRight) or (not self.mechFlipped and self.holdingLeft) then
	    tech.setAnimationState("movement", "hoverB")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "hoverB")
		mechAnimatePauldron("frontArmPauldronMovement", "hoverB")
		tech.setParticleEmitterActive("hoverFParticles", false)
		tech.setParticleEmitterActive("hoverParticles", false)
		tech.setParticleEmitterActive("hoverBParticles", true)
		tech.rotateGroup("frontHover", 0.17, true)
		tech.rotateGroup("backHover", 0.17, true)
	  elseif (self.mechFlipped and self.holdingLeft) or (not self.mechFlipped and self.holdingRight) then
	    tech.setAnimationState("movement", "hoverF")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "hoverF")
		mechAnimatePauldron("frontArmPauldronMovement", "hoverF")
		tech.setParticleEmitterActive("hoverFParticles", true)
		tech.setParticleEmitterActive("hoverParticles", false)
		tech.setParticleEmitterActive("hoverBParticles", false)
		tech.rotateGroup("frontHover", -0.17, true)
		tech.rotateGroup("backHover", -0.17, true)
	  else
	    tech.setAnimationState("movement", "hoverIdle")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "hoverIdle")
		mechAnimatePauldron("frontArmPauldronMovement", "hoverIdle")
		tech.setParticleEmitterActive("hoverFParticles", false)
		tech.setParticleEmitterActive("hoverParticles", true)
		tech.setParticleEmitterActive("hoverBParticles", false)
		tech.rotateGroup("frontHover", 0, true)
		tech.rotateGroup("backHover", 0, true)
	  end
	  -- Other anims
	  mechAnimateTorso("torso", "idle")
	  tech.setAnimationState("crotch", "idle")
	  self.fastDrop = false
	  self.veryFastDrop = false
	elseif self.isHovering then
	  self.jumpReleased = false
	  self.isHovering = false
	  self.hoverTimer = 0	
	  tech.setAnimationState("jetPack", "turnoff1")
	  tech.setAnimationState("jetFlame", "off")
	  tech.setAnimationState("hoverFlame", "off")
	  tech.setParticleEmitterActive("hoverFParticles", false)
	  tech.setParticleEmitterActive("hoverParticles", false)
	  tech.setParticleEmitterActive("hoverBParticles", false)
	end
	
	-- Skidding
    if math.abs(mcontroller.velocity()[1]) >= 30 and mcontroller.onGround() then
	  local mechCustomSkiddingMovementParameters = copy(tech.parameter("mechCustomMovementParameters"))
	  mechCustomSkiddingMovementParameters.normalGroundFriction = mechSkiddingFriction
	  mcontroller.controlParameters(mechCustomSkiddingMovementParameters) -- dynamic coefficient of friction, y'see....
	  tech.setParticleEmitterActive("skidParticles", true)
	  -- Spawn dust
	else 
	  tech.setParticleEmitterActive("skidParticles", false)
    end	  

	-- Landing Movement Suppression
	if self.immobileTimer > 0 and mcontroller.onGround() then
	  self.immobileTimer = self.immobileTimer - args.dt  
	  -- Anti-Skidding
	  if math.abs(mcontroller.velocity()[1]) < 15 then
		mcontroller.controlApproachXVelocity(0, 3000, true)
		tech.setParticleEmitterActive("skidParticles", false)
	  end 
	  -- Animation
	  if self.veryFastDrop or self.veryVeryFastDrop then
		tech.setAnimationState("movement", "landHard")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "idle")
		mechAnimatePauldron("frontArmPauldronMovement", "idle")
		mechAnimateTorso("torso", "idle")
		tech.setAnimationState("crotch", "idle")
	  elseif self.fastDrop then
		tech.setAnimationState("movement", "land")
		mechAnimateArms(mechLeftAnimation[1], mechLeftAnimation[2], mechRightAnimation[1], mechRightAnimation[2], "right", self.mechFlipped, "idle")
		mechAnimatePauldron("frontArmPauldronMovement", "idle")
		mechAnimateTorso("torso", "idle")
		tech.setAnimationState("crotch", "idle")
	  end
	  mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)	  
	elseif (self.movementSuppressed and self.immobileTimer <= 0) or (self.immobileTimer > 0 and not mcontroller.onGround()) then
	  -- tech.setParticleEmitterActive("skidParticles", false)
	  self.movementSuppressed = false
	  self.immobileTimer = 0
	  self.fastDrop = false
	  self.veryFastDrop = false
	  self.veryVeryFastDrop = false
	end
	
	-- Force walking (when attacking)
	if self.forceWalkGun then
	  self.forceWalk = true
	else
	  self.forceWalk = false
	end
	
	-- Timers
	if self.backHopTimer > 0 then
	  self.backHopTimer = self.backHopTimer - args.dt
	end
	if self.beamTimer > 0 then
	  self.beamTimer = self.beamTimer - args.dt
	end
	if self.hGunTimer > 0 then
	  self.hGunTimer = self.hGunTimer - args.dt
	end	  	  
	
	-- WEAPON: Torso Cannons
    if self.holdingAltFire and status.resource("energy") > energyCostPerAltShot + 1 then
      if self.fireTimer <= 0 then
	    local fireAngle = cannonAimAngle - mechCannonFireCone * math.random() + mechCannonFireCone * math.random()      		
		world.spawnProjectile(mechCannonProjectile, vec_add(mcontroller.position(), tech.anchorPoint("frontCannonFirePoint")), entity.id(), {math.cos(fireAngle), math.sin(fireAngle)}, false, mechCannonProjectileConfig)
		self.fireTimer = self.fireTimer + mechCannonFireCycle
        tech.setAnimationState("frontCannonFiring", "fire")
		tech.setAnimationState("frontRecoil", "fire")
		tech.playSound("mechCannonFireSound")
		energyCostAccumulated = energyCostAccumulated + energyCostPerAltShot
      else
        local oldFireTimer = self.fireTimer
		self.fireTimer = self.fireTimer - args.dt
        if oldFireTimer > mechCannonFireCycle / 2 and self.fireTimer <= mechCannonFireCycle / 2 then
          local fireAngle = cannonAimAngle - mechCannonFireCone * math.random() + mechCannonFireCone * math.random()
		  world.spawnProjectile(mechCannonProjectile, vec_add(mcontroller.position(), tech.anchorPoint("backCannonFirePoint")), entity.id(), {math.cos(fireAngle), math.sin(fireAngle)}, false, mechCannonProjectileConfig)	  
		  tech.setAnimationState("backCannonFiring", "fire")
		  tech.setAnimationState("backRecoil", "fire")
		  tech.playSound("mechCannonFireSound")
		  energyCostAccumulated = energyCostAccumulated + energyCostPerAltShot
        end
      end
    end

	-- WEAPON: Heat Beam
	if table.exists("heatBeam", mechEquip) then
	  -- Setup local variables left/right
	  local mechActiveSide = nil
	  local mechOtherSide = nil
	  local mechActiveAnim = {}
	  local mechOtherAnim = {}
	  local mechActiveSideButton = nil
	  if mechLeftEquip == "heatBeam" then
	    mechActiveSide = "left"
		mechOtherSide = "right"
		mechActiveAnim = mechLeftAnimation
		mechOtherAnim = mechRightAnimation
		mechActiveSideButton = self.holdingFire
	  elseif mechRightEquip == "heatBeam" then
	    mechActiveSide = "right"
		mechOtherSide = "left"
		mechActiveAnim = mechRightAnimation
		mechOtherAnim = mechLeftAnimation
		mechActiveSideButton = self.holdingAltFire
	  end  
	  
	  -- Keeps track of arm rotation
	  local firePoint = vec_add(mcontroller.position(), mechChooseObject(tech.anchorPoint("frontGunFirePoint"), tech.anchorPoint("backGunFirePoint"), mechActiveSide, self.mechFlipped))
	  local rotPoint = vec_add(mcontroller.position(), mechChooseObject(tech.anchorPoint("frontGunRotationPoint"), tech.anchorPoint("backGunRotationPoint"), mechActiveSide, self.mechFlipped))
	  local gunAngleDiff = world.distance(firePoint, rotPoint)
	  local gunAngle = math.atan2(gunAngleDiff[2], gunAngleDiff[1])
	  local gunAngleOffset = math.atan2(0.25, 5.125)
	  if self.mechFlipped then
		gunAngle = gunAngle - gunAngleOffset
	  else
		gunAngle = gunAngle + gunAngleOffset
	  end		  
	  
	  if mechActiveSideButton and (not self.attackQueued) and (self.hGunState == "limbo" or self.hGunState == "limbo2") then
	    self.attackQueued = true
	  end
	  
	  -- Weapon operation
	  if mechActiveSideButton and self.hGunState == "idle" then
	    self.hGunState = "readying"
		self.hGunTimer = mechGunReadyTime
		tech.playSound("mechGunReadySound")
		mechSetArmOccupied(mechActiveSide, true)
		self.forceWalkGun = true
		self.torsoOccupied = true
	  elseif self.hGunState == "readying" and self.hGunTimer <= 0 then
		self.hGunState = "limbo"
		self.hGunTimer = mechGunIdleTime
		tech.playSound("mechGunIdleSound")
	  elseif self.hGunState == "limbo" then
		if self.attackQueued and status.resource("energy") > energyCostPerBeamFire + 1 then
		  self.hGunState = "firing"
		  self.hGunTimer = mechGunFireTime
		  tech.playSound("mechGunFireSound")
		  energyCostAccumulated = energyCostAccumulated + energyCostPerBeamFire
		  -- mechAnimateActiveArm("frontFiring", "backFiring", "fire", mechActiveSide, self.mechFlipped)	
		elseif self.hGunTimer <= 0 then
		  self.hGunState = "holstering"
		  self.hGunTimer = mechGunHolsterTime
		  tech.playSound("mechGunHolsterSound")
	    end	  
	  elseif self.hGunState == "firing" then
	    self.gunRotationSuppressed = true
		self.attackQueued = false
	    -- Fire shot
		if self.beamTimer <= 0 then	  
		  -- Spawn beam
		  local endPoint = {math.cos(gunAngle) * mechGunBeamMaxRange + firePoint[1], math.sin(gunAngle) * mechGunBeamMaxRange + firePoint[2]}
		  local beamCollision = progressiveLineCollision(firePoint, endPoint, mechGunBeamStep)
		  randomProjectileLine(firePoint, beamCollision, mechGunBeamStep, mechGunBeamSmokeProkectile, 0.08)
		  randomProjectileLine(firePoint, beamCollision, mechGunBeamStep*4, mechGunLightProjectile, 1.00)
		  local beamDamage = status.resource("energy")
		  spawnVariableDamageBeam(firePoint, beamCollision, mechGunBeamWidth, mechGunBeamHitProjectile, mechGunBeamUpperDamage, mechGunBeamLowerDamage, mechGunBeamMaxRange)
		  world.spawnProjectile(mechGunBeamEndProjectile, beamCollision, entity.id(), {0, 0}, false)
		
		  -- Beam animation graphic
		  local scaleGroup = mechChooseObject("frontLaser", "backLaser", mechActiveSide, self.mechFlipped)
		  local scale = math.distance(firePoint, beamCollision)
		  tech.scaleGroup(scaleGroup, {scale, 1})
		  
		  self.beamTimer = mechGunBeamUpdateTime
		  self.mGunRecoilKick = self.mGunRecoilKick + mechGunRecoilKick
		  local diagX = -math.cos(gunAngle)
		  mcontroller.controlApproachXVelocity(mechGunRecoilSpeed * diagX, mechGunRecoilPower * math.abs(diagX), true)	
		end		
		if self.hGunTimer <= 0 then
		  mechAnimateActiveArm("frontLaser", "backLaser", "fade", mechActiveSide, self.mechFlipped)
		  mechAnimateActiveArm("frontLaser", "backLaser", "off", mechOtherSide, self.mechFlipped)	
		  self.hGunState = "cooling"
		  self.hGunTimer = mechGunCoolingTime
		  tech.playSound("mechGunCoolingSound")
		  self.mGunRecoilKick = 0
		end
	  elseif self.hGunState == "cooling" and self.hGunTimer <= 0 then
		self.hGunState = "limbo2"
		self.hGunTimer = 0.25
		self.gunRotationSuppressed = false
	  elseif self.hGunState == "limbo2" and self.hGunTimer <= 0 then
		self.hGunState = "limbo"
		self.hGunTimer = mechGunIdleTime/3
		tech.playSound("mechGunIdleSound")    
	  elseif self.hGunState == "holstering" and self.hGunTimer <= 0 then
		self.hGunState = "idle"
		self.forceWalkGun = false
		mechSetArmOccupied(mechActiveSide, false)
		self.torsoOccupied = false
		self.hGunTimer = 0	
		self.mGunRecoilKick = 0
	  end	
	  -- Pauldron Animation
	  if self.hGunState == "readying" then
	    if (mechActiveSide == "left" and self.mechFlipped) or (mechActiveSide == "right" and not self.mechFlipped) then
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "gunTurnOn", mechActiveSide, self.mechFlipped)
		  if self.mechFlipped then
		    tech.rotateGroup("frontArmPauldron", math.pi - gunAngle)	
		  else
		    tech.rotateGroup("frontArmPauldron", gunAngle)	
		  end
	    end
	  elseif self.hGunState == "limbo" or self.hGunState == "limbo2" or self.hGunState == "firing" or self.hGunState == "cooling" then
	    if (mechActiveSide == "left" and self.mechFlipped) or (mechActiveSide == "right" and not self.mechFlipped) then
		  local animAnglePauldron = {"rot.2", "rot.3", "rot.4", "rot.5", "rot.6", "rot.7", "rot.8", "rot.9"}
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, chooseAnimAngle(gunAngle, -mechGunAimLimit, mechGunAimLimit, animAnglePauldron, self.mechFlipped), mechActiveSide, self.mechFlipped)
		  tech.rotateGroup("frontArmPauldron", 0)	
	    end
	  elseif self.hGunState == "holstering" then
	    if (mechActiveSide == "left" and self.mechFlipped) or (mechActiveSide == "right" and not self.mechFlipped) then
		  mechAnimateActiveArm("frontArmPauldronMovement", nil, "gunTurnOff", mechActiveSide, self.mechFlipped)
		  if self.mechFlipped then
		    tech.rotateGroup("frontArmPauldron", math.pi - gunAngle)	
		  else
		    tech.rotateGroup("frontArmPauldron", gunAngle)	
		  end
	    end	
	  end
	  
	  -- Arm Animation
	  if self.hGunState == "readying" then
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "gunTurnOn", mechActiveSide, self.mechFlipped)
	    mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
	  elseif self.hGunState == "limbo" or self.hGunState == "limbo2" then
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "rotation", mechActiveSide, self.mechFlipped)
	    mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontLaser", "backLaser", "off", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontLaser", "backLaser", "off", mechOtherSide, self.mechFlipped)	
	  elseif self.hGunState == "firing" then
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "gunFire", mechActiveSide, self.mechFlipped)
	    mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)	
		mechAnimateActiveArm("frontFiring", "backFiring", "fire", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontFiring", "backFiring", "off", mechOtherSide, self.mechFlipped)		
		mechAnimateActiveArm("frontLaser", "backLaser", "fire", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontLaser", "backLaser", "off", mechOtherSide, self.mechFlipped)
	  elseif self.hGunState == "cooling" then
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "gunCool", mechActiveSide, self.mechFlipped)
	    mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontFiring", "backFiring", "off", mechActiveSide, self.mechFlipped)
		mechAnimateActiveArm("frontFiring", "backFiring", "off", mechOtherSide, self.mechFlipped)
	  elseif self.hGunState == "holstering" then
		mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "gunTurnOff", mechActiveSide, self.mechFlipped)
	    mechAnimateActiveArm(mechActiveAnim[1], mechActiveAnim[2], "off", mechOtherSide, self.mechFlipped)
		mechAnimateActiveArm(mechOtherAnim[1], mechOtherAnim[2], "off", mechActiveSide, self.mechFlipped)		
	  end
	  -- Torso Animation
	  if self.hGunState == "firing" then
	    if self.mechFlipped then
		  if mechActiveSide == "left" then
		    tech.setAnimationState("torso", "turnAway")
		  else
		    tech.setAnimationState("torso", "turnToward")
		  end
		else
		  if mechActiveSide == "left" then
		    tech.setAnimationState("torso", "turnToward")
		  else
		    tech.setAnimationState("torso", "turnAway")
		  end
		end
	  else
	    if self.torsoOccupied and self.mechFlipped then
	      if mechActiveSide == "left" then
		    tech.setAnimationState("torso", "idleAway")
		  else
		    tech.setAnimationState("torso", "idleToward")
		  end
	    elseif self.torsoOccupied and not self.mechFlipped then
	      if mechActiveSide == "left" then
		    tech.setAnimationState("torso", "idleToward")
	      else
		    tech.setAnimationState("torso", "idleAway")
		  end	  
	    end 
	  end
	end
	
    return 0 -- tech.consumeTechEnergy(energyCostPerSecond * args.dt + energyCostAccumulated)
  end  
  
  return 0
end
