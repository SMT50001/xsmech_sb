function init()
  self.active = false
  self.fireTimer = 0
  self.altFireTimer = 0
  self.altFireIntervalTimer = 0
  self.altFireCount = 0
  tech.setVisible(false)
  tech.rotateGroup("guns", 0, true)
  self.holdingJump = false
  self.jumpPressed = false
  self.jumpReleased = false
  self.holdingUp = false
  self.holdingDown = false
  self.holdingAltFire = false
  self.hoverTimer = 0
  self.bHasHovered = false
  self.bIsHovering = false
  self.fTracerCount = 0
  self.bTracerCount = 0
  self.mechFlipped = false
  
  -- Activate/Deactivate
  self.mechState = "off" -- Possible values "off" "turningOn" "on" "turningOff"
  self.mechStateTimer = 0
end

function uninit()
  if self.active then
    local mechTransformPositionChange = tech.parameter("mechTransformPositionChange")
    mcontroller.translate({-mechTransformPositionChange[1], -mechTransformPositionChange[2]})
    tech.setParentOffset({0, 0})
    self.active = false
    tech.setVisible(false)
    tech.setParentState()
    tech.setToolUsageSuppressed(false)
    mcontroller.controlFace(nil)
	
	-- Particles / Anims
	tech.setAnimationState("movement", "idle")
	tech.setAnimationState("hovering", "off")
	tech.setAnimationState("torso", "idle")
	tech.setAnimationState("frontFiring", "off")
	tech.setAnimationState("backFiring", "off")
	tech.setAnimationState("frontRecoil", "off")
	tech.setAnimationState("backRecoil", "off")
	tech.setAnimationState("missilePodRecoil", "off")
	tech.setParticleEmitterActive("hoverParticles", false)
  end
end

function input(args)
  -- Check if player is holding jump first
  if args.moves["jump"] then
    self.holdingJump = true
  elseif not args.moves["jump"] then
    self.holdingJump = false
  end
  
  -- Checks if jump was released
  if not self.jumpReleased then
    if self.holdingJump and not self.jumpPressed and not self.jumpReleased then
	  self.jumpPressed = true
    elseif not self.holdingJump and self.jumpPressed and not self.jumpReleased then
	  self.jumpReleased = true
	  self.jumpPressed = false
    end
  end
  
  if args.moves["up"] then
    self.holdingUp = true
  elseif not args.moves["up"] then
    self.holdingUp = false
  end
  
  if args.moves["down"] then
    self.holdingDown = true
  elseif not args.moves["down"] then
    self.holdingDown = false
  end
  
  if args.moves["altFire"] then
    self.holdingAltFire = true
  elseif not args.moves["altFire"] then
    self.holdingAltFire = false
  end

  if args.moves["special"] == 1 then
    if self.active then
      return "mechDeactivate"
    else
      return "mechActivate"
    end
  elseif args.moves["primaryFire"] then
    return "mechFire"
  end

  return nil
end

function update(args)
  local energyCostPerSecond = tech.parameter("energyCostPerSecond")
  local energyCostPerPrimaryShot = tech.parameter("energyCostPerPrimaryShot")
  local energyCostPerAltShot = tech.parameter("energyCostPerAltShot")
  local primaryShotsFired = 0
  local altShotsFired = 0
  
  local mechStartupTime = tech.parameter("mechStartupTime")
  local mechShutdownTime = tech.parameter("mechShutdownTime")
  
  local mechCustomMovementParameters = tech.parameter("mechCustomMovementParameters")
  local mechCustomTurningOnOffMovementParameters = copy(tech.parameter("mechCustomMovementParameters"))
  mechCustomTurningOnOffMovementParameters.runSpeed = 0.0
  mechCustomTurningOnOffMovementParameters.walkSpeed = 0.0
  local mechTransformPositionChange = tech.parameter("mechTransformPositionChange")
  local parentOffset = tech.parameter("parentOffset")
  local mechCollisionTest = tech.parameter("mechCollisionTest")
  local mechAimLimit = tech.parameter("mechAimLimit") * math.pi / 180
  local mechFrontRotationPoint = tech.parameter("mechFrontRotationPoint")
  local mechFrontFirePosition = tech.parameter("mechFrontFirePosition")
  local mechBackRotationPoint = tech.parameter("mechBackRotationPoint")
  local mechBackFirePosition = tech.parameter("mechBackFirePosition")
  local mechFireCycle = tech.parameter("mechFireCycle")
  local mechProjectile = tech.parameter("mechProjectile")
  local mechTracerProjectile = tech.parameter("mechTracerProjectile")
  local mechProjectileConfig = tech.parameter("mechProjectileConfig")
  
  local mechGunFireCone = tech.parameter("mechGunFireCone") * math.pi / 180
  
  local mechAltFireCycle = tech.parameter("mechAltFireCycle") 
  local mechAltProjectile = tech.parameter("mechAltProjectile")
  local mechAltProjectileConfig = tech.parameter("mechAltProjectileConfig")
  local mechAltFireShotInterval = tech.parameter("mechAltFireShotInterval")
  
  local mechHoverSpeed = tech.parameter("mechHoverSpeed")
  local mechHoverTime = tech.parameter("mechHoverTime")
  
  if not self.active and args.actions["mechActivate"] and self.mechState == "off" then
    mechCollisionTest[1] = mechCollisionTest[1] + mcontroller.position()[1]
    mechCollisionTest[2] = mechCollisionTest[2] + mcontroller.position()[2]
    mechCollisionTest[3] = mechCollisionTest[3] + mcontroller.position()[1]
    mechCollisionTest[4] = mechCollisionTest[4] + mcontroller.position()[2]
    if not world.rectCollision(mechCollisionTest) then
      tech.burstParticleEmitter("mechActivateParticles")
      mcontroller.translate(mechTransformPositionChange)
      tech.setVisible(true)
	    tech.setAnimationState("movement", "idle")
      tech.setParentState("sit")
      tech.setToolUsageSuppressed(true)

	    self.mechState = "turningOn"
	    self.mechStateTimer = mechStartupTime
	    self.active = true
	    tech.playSound("mechStartupSound")
	  
	  local diff = world.distance(tech.aimPosition(), mcontroller.position())
      local aimAngle = math.atan2(diff[2], diff[1])
      self.mechFlipped = aimAngle > math.pi / 2 or aimAngle < -math.pi / 2
    else
      -- Make some kind of error noise
    end
  elseif self.mechState == "turningOn" and self.mechStateTimer <= 0 then
    self.mechState = "on"
	self.mechStateTimer = 0
  elseif (self.active and (args.actions["mechDeactivate"] and self.mechState == "on" or (energyCostPerSecond * args.dt > status.resource("energy")) and self.mechState == "on")) then
	self.mechState = "turningOff"
	self.mechStateTimer = mechShutdownTime
	tech.playSound("mechShutdownSound")
  elseif self.mechState == "turningOff" and self.mechStateTimer <= 0 then 
    tech.burstParticleEmitter("mechDeactivateParticles")
	mcontroller.translate({-mechTransformPositionChange[1], -mechTransformPositionChange[2]})
    tech.setVisible(false)
    tech.setParentState()
    tech.setToolUsageSuppressed(false)
    tech.setParentOffset({0, 0})
	
	-- Particles / Anims
	tech.setAnimationState("movement", "idle")
	tech.setAnimationState("hovering", "off")
	tech.setAnimationState("torso", "idle")
	tech.setAnimationState("frontFiring", "off")
	tech.setAnimationState("backFiring", "off")
	tech.setAnimationState("frontRecoil", "off")
	tech.setAnimationState("backRecoil", "off")
	tech.setAnimationState("missilePodRecoil", "off")
	tech.setParticleEmitterActive("hoverParticles", false)
	
	self.jumpPressed = false
	self.jumpReleased = false
    self.hoverTimer = 0
    -- self.bHasHovered = false
    self.bIsHovering = false
	self.mechState = "off"
	self.mechStateTimer = 0
	
    self.active = false
  end

  mcontroller.controlFace(nil)
  
  if self.mechStateTimer > 0 then
	self.mechStateTimer = self.mechStateTimer - args.dt
  end

 local diff = world.distance(tech.aimPosition(), mcontroller.position())
 local aimAngle = math.atan2(diff[2], diff[1])
 local flip = aimAngle > math.pi / 2 or aimAngle < -math.pi / 2
  
  -- Basic animation
  if self.active then
	if self.mechState == "on" then
		if flip then  
		  self.mechFlipped = true
		else
		  self.mechFlipped = false
		end
	end	
	if self.mechFlipped then
	  tech.setFlipped(true)  			  
	  local nudge = tech.appliedOffset()
	  tech.setParentOffset({-parentOffset[1] - nudge[1], parentOffset[2] + nudge[2]})
	  mcontroller.controlFace(-1)	
      if aimAngle > 0 then
        aimAngle = math.max(aimAngle, math.pi - mechAimLimit)
      else
        aimAngle = math.min(aimAngle, -math.pi + mechAimLimit)
      end
	  tech.rotateGroup("guns", math.pi - aimAngle)	  
	else
	  tech.setFlipped(false)
	  local nudge = tech.appliedOffset()
	  tech.setParentOffset({parentOffset[1] + nudge[1], parentOffset[2] + nudge[2]})
	  mcontroller.controlFace(1)	
      if aimAngle > 0 then
        aimAngle = math.min(aimAngle, mechAimLimit)
      else
        aimAngle = math.max(aimAngle, -mechAimLimit)
      end	  
	  tech.rotateGroup("guns", aimAngle)
	end  
  end
  
  -- Startup, shutdown, and active
  if self.mechState == "turningOn" then
	mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)
	tech.setAnimationState("movement", "stand")
	tech.setAnimationState("torso", "turnOn")   
  elseif self.mechState == "turningOff" then
	mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)
	tech.setAnimationState("movement", "sit")
	tech.setAnimationState("torso", "turnOff")	   
  elseif self.active and self.mechState == "on" then
  mcontroller.controlParameters(mechCustomMovementParameters)
	tech.setAnimationState("torso", "idle")	   
    if not mcontroller.onGround() then
	  if self.holdingJump and self.jumpReleased and not self.bHasHovered then
	    -- Activate hovering
	    self.bIsHovering = true
		self.hoverTimer = mechHoverTime
		self.bHasHovered = true
		self.jumpReleased = false
	  elseif mcontroller.velocity()[2] > 0 and not self.bIsHovering then
        tech.setAnimationState("movement", "jump")
	  elseif self.bIsHovering and self.hoverTimer > 0 then
	    -- Maintain hovering
		tech.setAnimationState("movement", "jump")
		if self.holdingUp and not self.holdingDown then
		  mcontroller.controlApproachYVelocity(5, 5000, false)
		elseif self.holdingDown and not self.holdingUp then
	      mcontroller.controlApproachYVelocity(-5, 5000, false)
		else
		  mcontroller.controlApproachYVelocity(0, 1000, true)
		end			
		-- Hover Timer
		self.hoverTimer = self.hoverTimer - args.dt			
		if self.jumpReleased and self.holdingJump then
			-- Deactivate hovering if jump is pressed again
			self.hoverTimer = 0
			self.bIsHovering = false
			self.jumpReleased = false
			tech.setAnimationState("movement", "fall")	
		end		
	  elseif self.bIsHovering and self.hoverTimer <= 0 then
		-- Deactivate hovering if hover time runs out
		self.jumpReleased = false
		self.hoverTimer = 0
		self.bIsHovering = false
		tech.setAnimationState("movement", "fall")	
      else
        tech.setAnimationState("movement", "fall")
		self.jumpReleased = true
      end
	elseif mcontroller.onGround() and self.bHasHovered then
	  -- Deactivate hovering if on the ground, reset hover ability
	  self.bHasHovered = false
	  self.bIsHovering = false
	  self.jumpReleased = false
    elseif mcontroller.walking() or mcontroller.running() then
	  self.jumpReleased = false
      if self.bIsHovering then
	    -- Deactivate hovering if walking/running
		self.hoverTimer = 0
		self.bIsHovering = false
		self.bHasHovered = false
	  end 
	  if self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1 then
        tech.setAnimationState("movement", "backWalk")
      else
        tech.setAnimationState("movement", "walk")
      end
    else
	  self.jumpReleased = false
	  -- Crouching
	  if self.holdingDown then
		tech.setAnimationState("movement", "sit")
	  else
		tech.setAnimationState("movement", "idle")
	  end
    end
	
	-- If hovering, activate hover animations, sound, and particle effects. Otherwise deactivate.
	if self.bIsHovering then
      tech.setAnimationState("hovering", "on")
      tech.setParticleEmitterActive("hoverParticles", true)
    else
      tech.setAnimationState("hovering", "off")
      tech.setParticleEmitterActive("hoverParticles", false)
    end
	
	-- Temp fall protection
	if mcontroller.velocity()[2] < -70 then
		mcontroller.controlApproachYVelocity(-70, 6000, false)
	end

	-- Primary weapon system (Miniguns)
    if args.actions["mechFire"] and status.resource("energy") > energyCostPerPrimaryShot then
      if self.fireTimer <= 0 then
	    local fireAngle = aimAngle - mechGunFireCone * math.random() + mechGunFireCone * math.random()      		
		-- Front Gun Tracer Counter
		if self.fTracerCount < 4 then
		  world.spawnProjectile(mechProjectile, vec_add(mcontroller.position(), tech.anchorPoint("frontGunFirePoint")), entity.id(), {math.cos(fireAngle), math.sin(fireAngle)}, false, mechProjectileConfig)
          self.fTracerCount = self.fTracerCount + 1
		else
		  world.spawnProjectile(mechTracerProjectile, vec_add(mcontroller.position(), tech.anchorPoint("frontGunFirePoint")), entity.id(), {math.cos(fireAngle), math.sin(fireAngle)}, false, mechProjectileConfig)
		  self.fTracerCount = 0
		end
		self.fireTimer = self.fireTimer + mechFireCycle
        tech.setAnimationState("frontFiring", "fire")
		tech.setAnimationState("frontRecoil", "fire")
		tech.playSound("mechFireSound")
		primaryShotsFired = primaryShotsFired + 1
      else
        local oldFireTimer = self.fireTimer
        self.fireTimer = self.fireTimer - args.dt
        if oldFireTimer > mechFireCycle / 2 and self.fireTimer <= mechFireCycle / 2 then
          local fireAngle = aimAngle - mechGunFireCone + math.random() * 2 * mechGunFireCone
		  -- Back Gun Tracer Counter
		  if self.bTracerCount < 4 then
		    world.spawnProjectile(mechProjectile, vec_add(mcontroller.position(), tech.anchorPoint("backGunFirePoint")), entity.id(), {math.cos(fireAngle), math.sin(fireAngle)}, false, mechProjectileConfig)
            self.bTracerCount = self.bTracerCount + 1
		  else
		    world.spawnProjectile(mechTracerProjectile, vec_add(mcontroller.position(), tech.anchorPoint("backGunFirePoint")), entity.id(), {math.cos(fireAngle), math.sin(fireAngle)}, false, mechProjectileConfig)
            self.bTracerCount = 0
		  end		  
		  tech.setAnimationState("backFiring", "fire")
		  tech.setAnimationState("backRecoil", "fire")
		  tech.playSound("mechFireSound2")
		  primaryShotsFired = primaryShotsFired + 1
        end
      end
    end

	-- Secondary weapon system (Missile Pod)
	if self.altFireTimer <= 0 and self.altFireCount <= 0 then
	  tech.setAnimationState("missilePodRecoil", "off")
      if self.holdingAltFire and status.resource("energy") > energyCostPerAltShot then
	    -- Prime pod for firing
	    self.altFireIntervalTimer = 0 -- mechAltFireShotInterval
	    self.altFireCount = 1
		self.altFireTimer = mechAltFireCycle
	  end
	elseif self.altFireTimer <= 1.0 and self.altFireCount >= 5 then
	  -- Reloading animation, reset weapon system
	  tech.setAnimationState("missilePodRecoil", "reload")
	  self.altFireCount = 0
	end
	-- Advance Timers
	if self.altFireTimer > 0 then
	  self.altFireTimer = self.altFireTimer - args.dt
	end	
	if self.altFireIntervalTimer > 0 then
	  self.altFireIntervalTimer = self.altFireIntervalTimer - args.dt	
	end
    -- Missile barrage	
	if self.altFireIntervalTimer <= 0 and self.altFireCount > 0 and self.altFireCount < 5 then
	  self.altFireIntervalTimer = mechAltFireShotInterval
	  if self.altFireCount == 1 then
	    world.spawnProjectile(mechAltProjectile, vec_add(mcontroller.position(), tech.anchorPoint("missilePodFirePoint1")), entity.id(), {math.sign(math.cos(aimAngle)) * 0.754, 0.656}, false, mechAltProjectileConfig)
        tech.setAnimationState("missilePodRecoil", "fire1")
		tech.playSound("mechAltFireSound")	
	  elseif self.altFireCount == 2 then
	    world.spawnProjectile(mechAltProjectile, vec_add(mcontroller.position(), tech.anchorPoint("missilePodFirePoint2")), entity.id(), {math.sign(math.cos(aimAngle)) * 0.731, 0.682}, false, mechAltProjectileConfig)
        tech.setAnimationState("missilePodRecoil", "fire2")
		tech.playSound("mechAltFireSound")	
	  elseif self.altFireCount == 3 then
	    world.spawnProjectile(mechAltProjectile, vec_add(mcontroller.position(), tech.anchorPoint("missilePodFirePoint3")), entity.id(), {math.sign(math.cos(aimAngle)) * 0.707, 0.707}, false, mechAltProjectileConfig)
        tech.setAnimationState("missilePodRecoil", "fire3")
		tech.playSound("mechAltFireSound")		
	  elseif self.altFireCount == 4 then
	    world.spawnProjectile(mechAltProjectile, vec_add(mcontroller.position(), tech.anchorPoint("missilePodFirePoint4")), entity.id(), {math.sign(math.cos(aimAngle)) * 0.682, 0.731}, false, mechAltProjectileConfig)
        tech.setAnimationState("missilePodRecoil", "fire4")
		tech.playSound("mechAltFireSound")	
	  end
	  self.altFireCount = self.altFireCount + 1
	  altShotsFired = altShotsFired + 1
	end

    return 0 -- tech.consumeTechEnergy(energyCostPerSecond * args.dt + altShotsFired * energyCostPerAltShot + primaryShotsFired * energyCostPerPrimaryShot)
  end

  return 0
end
