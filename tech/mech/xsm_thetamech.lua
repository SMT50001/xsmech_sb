function init()
  self.active = false
  self.fireTimer = 0
  self.altFireTimer = 0
  self.altFireIntervalTimer = 0
  self.altFireCount = 0
  tech.setVisible(false)
  tech.rotateGroup("guns", 0, true)
  self.mechFlipped = false
  
  -- Activate/Deactivate
  self.mechState = "off" -- Possible values "off" "turningOn" "on" "turningOff"
  self.mechStateTimer = 0
  
  -- Init input variables
  self.holdingJump = false
  self.holdingUp = false
  self.holdingDown = false
  self.holdingAltFire = false
  
  -- Init boost variables
  self.boostPower = 0
  self.boostTimer = 0
  self.boostFireTimer = 0
  self.boostCharging = false
  self.boostFiring = false
  self.boostRecharging = false
  tech.setParticleEmitterActive("boostParticles1", false)
  tech.setParticleEmitterActive("boostParticles2", false)
  tech.setParticleEmitterActive("boostParticles3", false)
  tech.setParticleEmitterActive("boostParticles4", false)
  tech.setParticleEmitterActive("emergencyBoostParticles", false)
  self.boostEmergency = false
  
  -- Init landing variables
  self.fastDrop = false
  self.veryFastDrop = false
  self.movementSuppressed = false
  self.immobileTimer = 0
end

function uninit()
  if self.active then
    local mechTransformPositionChange = tech.parameter("mechTransformPositionChange")
    mcontroller.translate({-mechTransformPositionChange[1], -mechTransformPositionChange[2]})
    tech.setParentOffset({0, 0})
    self.active = false
    tech.setVisible(false)
    tech.setParentState()
    tech.setToolUsageSuppressed(false)
    mcontroller.controlFace(nil)
	
	-- Anims, particles
	tech.setParticleEmitterActive("boostParticles1", false)
	tech.setParticleEmitterActive("boostParticles2", false)
	tech.setParticleEmitterActive("boostParticles3", false)
	tech.setParticleEmitterActive("boostParticles4", false)
	tech.setParticleEmitterActive("emergencyBoostParticles", false)
	tech.setParticleEmitterActive("hardLandingParticles", false)
	tech.setParticleEmitterActive("veryHardLandingParticles", false)
	tech.setAnimationState("emergencyJets", "off")	
	tech.setAnimationState("frontFiring", "off")	
	tech.setAnimationState("backFiring", "off")	
	tech.setAnimationState("boost", "off")	
	tech.setAnimationState("torso", "idle")	
	tech.setAnimationState("grenadeBox", "idle")
	tech.setAnimationState("movement", "idle")
  end
end

function input(args)
  -- Check if player is holding jump etc first
  if args.moves["jump"] then
    self.holdingJump = true
  elseif not args.moves["jump"] then
    self.holdingJump = false
  end
  
  if args.moves["up"] then
    self.holdingUp = true
  elseif not args.moves["up"] then
    self.holdingUp = false
  end
  
  if args.moves["down"] then
    self.holdingDown = true
  elseif not args.moves["down"] then
    self.holdingDown = false
  end
  
  if args.moves["altFire"] then
    self.holdingAltFire = true
  elseif not args.moves["altFire"] then
    self.holdingAltFire = false
  end

  if args.moves["special"] == 1 then
    if self.active then
      return "mechDeactivate"
    else
      return "mechActivate"
    end
  elseif args.moves["primaryFire"] then
    return "mechFire"
  end

  return nil
end

function spawnPenetratingBeam(startPoint, endPoint, penetration, stepSize, projectile, projectileConfig, projectilePen, projectilePenConfig)
  local dist = math.distance(startPoint, endPoint)
  local steps = math.floor(dist / stepSize)
  local normX = (endPoint[1] - startPoint[1]) / dist
  local normY = (endPoint[2] - startPoint[2]) / dist
  local penSteps = math.floor(penetration / stepSize)
  local pen = 0
  for i = 0, steps do
    local p1 = { normX * i * stepSize + startPoint[1], normY * i * stepSize + startPoint[2]}
	local p2 = { normX * (i + 1) * stepSize + startPoint[1], normY * (i + 1) * stepSize + startPoint[2]}
	if projectile ~= nil then            
	  world.spawnProjectile(projectile, math.midPoint(p1, p2), entity.id(), {normX, normY}, false, projectileConfig)
	end
	if world.lineTileCollision(p1, p2, "Dynamic") then
	  pen = pen + 1
	  world.spawnProjectile(projectilePen, math.midPoint(p1, p2), entity.id(), {normX, normY}, false, projectilePenConfig)
	end
	if pen >= penSteps then
	  return math.midPoint(p1, p2)
	end	
  end
  world.spawnProjectile(projectilePen, endPoint, entity.id(), {normX, normY}, false, projectilePenConfig)
  return endPoint
end

function spawnDamageBeam(startPoint, endPoint, beamWidth, damageProjectile, damageProjectileConfig)
  local queryStart = startPoint
  local queryEnd = endPoint
  if startPoint[1] > endPoint[1] then
	queryStart = {endPoint[1], queryStart[2]}
	queryEnd = {startPoint[1], queryEnd[2]}
  end
  if startPoint[2] > endPoint[2] then
	queryStart = {queryStart[1], endPoint[2]}
	queryEnd = {queryEnd[1], startPoint[2]}
  end  
  local entityTable = world.entityQuery(queryStart, queryEnd)
  local enemiesHit = 0
  for _, v in ipairs(entityTable) do
	if world.entityType(v) == "player" or world.entityType(v) == "npc" or world.entityType(v) == "monster" then
	  local pos = world.entityPosition(v)
	  local distdX = startPoint[1] - endPoint[1]
	  local distdY = startPoint[2] - endPoint[2]
	  local d = math.abs( distdY * pos[1] - distdX * pos[2] + startPoint[1] * endPoint[2] - startPoint[2] * endPoint[1] ) / math.sqrt( (distdX)^2 + (distdY)^2 )
	  if d <= beamWidth / 2 then
		world.spawnProjectile(damageProjectile, pos, entity.id(), {0, 0}, false, damageProjectileConfig)
		enemiesHit = enemiesHit + 1
	  end
	end
  end
  return enemiesHit
end

function update(args)
  -- Energy Usage
  local energyCostPerSecond = tech.parameter("energyCostPerSecond")
  local energyCostPerPrimaryShot = tech.parameter("energyCostPerPrimaryShot")
  local energyCostPerAltShot = tech.parameter("energyCostPerAltShot")
  local energyCostPerBoost = tech.parameter("energyCostPerBoost")
  local energyCostPerSecondEmergencyJets = tech.parameter("energyCostPerSecondEmergencyJets")
  local primaryShotsFired = 0
  local altShotsFired = 0
  local boostShotsFired = 0   
  
  -- Misc
  local statusEffects = tech.parameter("statusEffects")
  local mechCustomMovementParameters = tech.parameter("mechCustomMovementParameters")
  local mechCustomTurningOnOffMovementParameters = copy(tech.parameter("mechCustomMovementParameters"))
  mechCustomTurningOnOffMovementParameters.runSpeed = 0.0
  mechCustomTurningOnOffMovementParameters.walkSpeed = 0.0
  local mechTransformPositionChange = tech.parameter("mechTransformPositionChange")
  local parentOffset = tech.parameter("parentOffset")
  local mechCollisionTest = tech.parameter("mechCollisionTest")
  local mechAimLimit = tech.parameter("mechAimLimit") * math.pi / 180
  local mechFrontRotationPoint = tech.parameter("mechFrontRotationPoint")
  local mechFrontFirePosition = tech.parameter("mechFrontFirePosition")
  local mechBackRotationPoint = tech.parameter("mechBackRotationPoint")
  local mechBackFirePosition = tech.parameter("mechBackFirePosition")
  local mechLandingProjectile = tech.parameter("mechLandingProjectile")
  local mechStartupTime = tech.parameter("mechStartupTime")
  local mechShutdownTime = tech.parameter("mechShutdownTime")
  
  -- Primary Fire
  local mechFireCycle = tech.parameter("mechFireCycle")
  local mechFireAnimTime = tech.parameter("mechFireAnimTime")
  local mechGunOffsetFront = tech.parameter("mechGunOffsetFront")
  local mechGunOffsetBack = tech.parameter("mechGunOffsetBack")
  local mechBeamProjectile = tech.parameter("mechBeamProjectile")  
  local mechBeamLongProjectile = tech.parameter("mechBeamLongProjectile")  
  local mechBeamEndProjectile = tech.parameter("mechBeamEndProjectile")
  local mechBeamProjectileConfig = tech.parameter("mechBeamProjectileConfig")  
  local mechBeamEndProjectileConfig = tech.parameter("mechBeamEndProjectileConfig")
  local mechBeamRange = tech.parameter("mechBeamRange")
  local mechBeamPen = tech.parameter("mechBeamPen")
  local mechBeamStep = tech.parameter("mechBeamStep")
  local mechBeamLongStep = tech.parameter("mechBeamLongStep")
  local mechBeamWidth = tech.parameter("mechBeamWidth")
  local mechRecoilSpeed = tech.parameter("mechRecoilSpeed")
  local mechRecoilPower = tech.parameter("mechRecoilPower")
  
  -- Alt Fire
  local mechAltFireCycle = tech.parameter("mechAltFireCycle") 
  local mechAltProjectile = tech.parameter("mechAltProjectile")
  local mechAltProjectileConfig = tech.parameter("mechAltProjectileConfig")
  local mechAltFireShotInterval = tech.parameter("mechAltFireShotInterval")
  
  -- Boost parameters
  local mechBoostPower = tech.parameter("mechBoostPower")
  local mechBoostSpeed = tech.parameter("mechBoostSpeed")
  local mechBoostDuration = tech.parameter("mechBoostDuration")
  local mechBoostChargeCoefficient = tech.parameter("mechBoostChargeCoefficient")
  local mechBoostChargeInterval = tech.parameter("mechBoostChargeInterval")
  local mechBoostRechargeTime = tech.parameter("mechBoostRechargeTime")
  local mechBoostAngle1 = tech.parameter("mechBoostAngle1") * math.pi / 180
  local mechBoostAngle2 = tech.parameter("mechBoostAngle2") * math.pi / 180
  local mechBoostAngle3 = tech.parameter("mechBoostAngle3") * math.pi / 180
  local mechBoostAngle4 = tech.parameter("mechBoostAngle4") * math.pi / 180 
  local mechEmergencyBoostSpeed = tech.parameter("mechEmergencyBoostSpeed")
  local mechEmergencyBoostPower = tech.parameter("mechEmergencyBoostPower")
  
    
  if not self.active and args.actions["mechActivate"] and self.mechState == "off" and mcontroller.onGround() then
    mechCollisionTest[1] = mechCollisionTest[1] + mcontroller.position()[1]
    mechCollisionTest[2] = mechCollisionTest[2] + mcontroller.position()[2]
    mechCollisionTest[3] = mechCollisionTest[3] + mcontroller.position()[1]
    mechCollisionTest[4] = mechCollisionTest[4] + mcontroller.position()[2]
    if (not world.rectCollision(mechCollisionTest)) then
      tech.burstParticleEmitter("mechActivateParticles")
      mcontroller.translate(mechTransformPositionChange)
      tech.setVisible(true)
	  tech.setAnimationState("movement", "idle")
      tech.setParentState("sit")
      tech.setToolUsageSuppressed(true)
	  
	  self.mechState = "turningOn"
	  self.mechStateTimer = mechStartupTime
	  self.active = true
	  tech.playSound("mechStartupSound")
	  
	  local diff = world.distance(tech.aimPosition(), mcontroller.position())
	  local aimAngle = math.atan2(diff[2], diff[1])
	  self.mechFlipped = aimAngle > math.pi / 2 or aimAngle < -math.pi / 2  
	  if self.mechFlipped then  
	    self.mechFlipped = true
	    tech.setAnimationState("arms", "gunBack")
	    tech.setAnimationState("armsAlt", "gunBack")  
	  else
	    self.mechFlipped = false
	    tech.setAnimationState("arms", "gunFront")
	    tech.setAnimationState("armsAlt", "gunFront")
	  end
	  
    else
      -- Make some kind of error noise
    end
  elseif self.mechState == "turningOn" and self.mechStateTimer <= 0 then
    self.mechState = "on"
	self.mechStateTimer = 0
  elseif (self.active and (args.actions["mechDeactivate"] and self.mechState == "on" and mcontroller.onGround()) or (energyCostPerSecond * args.dt > status.resource("energy")) and self.mechState == "on") then
	self.mechState = "turningOff"
	self.mechStateTimer = mechShutdownTime
	tech.playSound("mechShutdownSound")
  elseif self.mechState == "turningOff" and self.mechStateTimer <= 0 then 
    tech.burstParticleEmitter("mechDeactivateParticles")
    mcontroller.translate({-mechTransformPositionChange[1], -mechTransformPositionChange[2]})
    tech.setVisible(false)
    tech.setParentState()
    tech.setToolUsageSuppressed(false)
    tech.setParentOffset({0, 0})
	
	-- Input
	self.holdingJump = false
	
	-- Boost
	self.boostPower = 0
	self.boostTimer = 0
	self.boostCharging = false
	self.boostFiring = false
	self.boostFireTimer = 0
	self.boostRecharging = false
	self.boostEmergency = false	
	
	-- Landing
	self.fastDrop = false
	self.veryFastDrop = false
	self.movementSuppressed = false
	self.immobileTimer = 0
	
	-- Anims, particles
	tech.setParticleEmitterActive("boostParticles1", false)
	tech.setParticleEmitterActive("boostParticles2", false)
	tech.setParticleEmitterActive("boostParticles3", false)
	tech.setParticleEmitterActive("boostParticles4", false)
	tech.setParticleEmitterActive("emergencyBoostParticles", false)
	tech.setParticleEmitterActive("hardLandingParticles", false)
	tech.setParticleEmitterActive("veryHardLandingParticles", false)
	tech.setAnimationState("emergencyJets", "off")	
	tech.setAnimationState("frontFiring", "off")	
	tech.setAnimationState("backFiring", "off")	
	tech.setAnimationState("boost", "off")	
	tech.setAnimationState("torso", "idle")	
	tech.setAnimationState("grenadeBox", "idle")
	tech.setAnimationState("movement", "idle")
	
	-- Weapons
	self.altFireIntervalTimer = 0
	self.altFireCount = 0
	
	self.mechState = "off"
	self.mechStateTimer = 0		
    self.active = false
  end
  
  mcontroller.controlFace(nil)
  
  if self.mechStateTimer > 0 then
	self.mechStateTimer = self.mechStateTimer - args.dt
  end

  local diff = world.distance(tech.aimPosition(), mcontroller.position())
  local aimAngle = math.atan2(diff[2], diff[1])
  local flip = aimAngle > math.pi / 2 or aimAngle < -math.pi / 2  
  local gunPosition = {0, 0}
  if flip then
	gunPosition = { mcontroller.position()[1] - mechGunOffsetBack[1], mcontroller.position()[2] + mechGunOffsetBack[2] }
  else
	gunPosition = { mcontroller.position()[1] + mechGunOffsetFront[1], mcontroller.position()[2] + mechGunOffsetFront[2] }
  end
  local diffCorrected = world.distance(tech.aimPosition(), gunPosition)
  local gunAngle = math.atan2(diffCorrected[2], diffCorrected[1])
	
  -- Basic animation
  if self.active then
    mcontroller.controlParameters(mechCustomMovementParameters)

	if not self.boostFiring and self.fireTimer <= mechFireCycle - mechFireAnimTime and self.mechState == "on" then
		if flip then  
		  self.mechFlipped = true
		  tech.setAnimationState("arms", "gunBack")
		  tech.setAnimationState("armsAlt", "gunBack")  
		else
		  self.mechFlipped = false
		  tech.setAnimationState("arms", "gunFront")
		  tech.setAnimationState("armsAlt", "gunFront")
		end
	end	
	if self.mechFlipped then
	  tech.setFlipped(true)
	  local nudge = tech.appliedOffset()
	  tech.setParentOffset({-parentOffset[1] - nudge[1], parentOffset[2] + nudge[2]})
	  mcontroller.controlFace(-1)

	  if gunAngle > 0 then
		gunAngle = math.max(gunAngle, math.pi - mechAimLimit)
	  else
		gunAngle = math.min(gunAngle, -math.pi + mechAimLimit)
	  end
	  
	  if self.mechState == "on" then
	    tech.rotateGroup("guns", math.pi - gunAngle)
	  else
	    tech.rotateGroup("guns", 0)
	  end
	  
	else
	  tech.setFlipped(false)
	  local nudge = tech.appliedOffset()
	  tech.setParentOffset({parentOffset[1] + nudge[1], parentOffset[2] + nudge[2]})
	  mcontroller.controlFace(1)

	  if gunAngle > 0 then
		gunAngle = math.min(gunAngle, mechAimLimit)
	  else
		gunAngle = math.max(gunAngle, -mechAimLimit)
	  end
	  
	  if self.mechState == "on" then
	    tech.rotateGroup("guns", gunAngle)
	  else
	    tech.rotateGroup("guns", 0)
	  end
	  
	end  
  end 
  
  -- Startup, shutdown, and active
  if self.mechState == "turningOn" then
	mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)
	tech.setAnimationState("movement", "stand")
	tech.setAnimationState("torso", "turnOn")   
  elseif self.mechState == "turningOff" then
	mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)
	tech.setAnimationState("movement", "sit")
	tech.setAnimationState("torso", "turnOff")	   
  elseif self.active and self.mechState == "on" then   
    
	-- Hard landings and very hard landings suppress movement temporarily
    if not mcontroller.onGround() then
      if mcontroller.velocity()[2] > 0 then
        tech.setAnimationState("movement", "jump")
		tech.setAnimationState("torso", "jump")
		self.fastDrop = false
	    self.veryFastDrop = false
      elseif not self.movementSuppressed then
        tech.setAnimationState("movement", "fall")
		tech.setAnimationState("torso", "jump")
		if mcontroller.velocity()[2] < -20 and mcontroller.velocity()[2] > -42 then
		  self.fastDrop = true
		elseif mcontroller.velocity()[2] <= -42 then
		  self.fastDrop = false
		  self.veryFastDrop = true
		end	
      end
	elseif self.fastDrop and mcontroller.onGround() then
      -- Hard landing
	  self.immobileTimer = 0.33 -- Temp
	  self.movementSuppressed = true
	  tech.setAnimationState("movement", "land")
	  tech.setAnimationState("torso", "land")
	  tech.burstParticleEmitter("hardLandingParticles")
    elseif self.veryFastDrop and mcontroller.onGround() then
      -- Very hard landing	  
	  self.immobileTimer = 0.66 -- Temp
	  self.movementSuppressed = true
	  tech.setAnimationState("movement", "landHard")
	  tech.setAnimationState("torso", "landHard")
	  tech.burstParticleEmitter("veryHardLandingParticles")
	  world.spawnProjectile(mechLandingProjectile, vec_add(mcontroller.position(), tech.anchorPoint("frontLegFoot")), entity.id(), {0, 0}, false)
	  world.spawnProjectile(mechLandingProjectile, vec_add(mcontroller.position(), tech.anchorPoint("backLegFoot")), entity.id(), {0, 0}, false)
    elseif (mcontroller.walking() or mcontroller.running()) and not self.fastDrop and not self.veryFastDrop and not self.movementSuppressed then
	  self.fastDrop = false
	  self.veryFastDrop = false
      if self.mechFlipped and mcontroller.movingDirection() == 1 or not self.mechFlipped and mcontroller.movingDirection() == -1 then
        tech.setAnimationState("movement", "backWalk")
		tech.setAnimationState("torso", "backWalk")
      else
        tech.setAnimationState("movement", "walk")
		tech.setAnimationState("torso", "walk")
      end
	elseif self.fastDrop or self.veryFastDrop or self.movementSuppressed then
	  -- do nothing
    else
	  self.fastDrop = false
	  self.veryFastDrop = false
      tech.setAnimationState("movement", "idle")
	  tech.setAnimationState("torso", "idle")
    end
	
	-- Landing Movement Suppression
	if self.immobileTimer > 0 then
	  self.immobileTimer = self.immobileTimer - args.dt
	  -- world.callScriptedEntity(entity.id(), "mcontroller.setVelocity", {0,0})
	  mcontroller.controlApproachXVelocity(0, 3000, true)
	  self.fastDrop = false
	  self.veryFastDrop = false
	  mcontroller.controlParameters(mechCustomTurningOnOffMovementParameters)
	elseif self.movementSuppressed and self.immobileTimer <= 0 then
	  self.movementSuppressed = false
	  self.immobileTimer = 0
	  self.fastDrop = false
	  self.veryFastDrop = false
	end
	
	-- Emergency Jets fire automatically when falling too fast (for later: integrate energy usage?)
	if not mcontroller.onGround() and mcontroller.velocity()[2] <= -70 and not self.boostEmergency then
	  self.boostEmergency = true
	  tech.setParticleEmitterActive("emergencyBoostParticles", true)
	  tech.playSound("mechBoostIgniteSound")
	end
	if self.boostEmergency and mcontroller.velocity()[2] <= -40 then
	  tech.setAnimationState("emergencyJets", "on")
	  local eDiagX = math.cos(mechBoostAngle1 + math.pi/2)
	  local eDiagY = math.sin(mechBoostAngle1 + math.pi/2)
	  self.boostDirection = math.sign(math.cos(aimAngle))
	  mcontroller.controlApproachVelocity({self.boostDirection * mechEmergencyBoostSpeed * eDiagX, mechEmergencyBoostSpeed * eDiagY}, mechEmergencyBoostPower, true, true)
	else
	  tech.setAnimationState("emergencyJets", "off")
	  self.boostEmergency = false
	  tech.setParticleEmitterActive("emergencyBoostParticles", false)
	end	
		
	-- Mech Boost
	-- Charges while holding alt fire, from level 1 to level 4.
	if self.holdingJump and not self.holdingDown and not self.boostFiring and not self.boostRecharging then
	  if not self.boostCharging then
	    -- Begin charging
		if status.resource("energy") > energyCostPerBoost then
		  self.boostCharging = true
		  self.boostPower = 1
		  self.boostTimer = mechBoostChargeInterval
		  tech.playSound("mechBoostChargeSound")
          boostShotsFired = boostShotsFired + 1		  
		end
      elseif self.boostTimer > 0 then
	    self.boostTimer = self.boostTimer - args.dt
	  elseif self.boostTimer <= 0 and self.boostPower < 4 and status.resource("energy") > energyCostPerBoost then
	    self.boostPower = self.boostPower + 1
		self.boostTimer = mechBoostChargeInterval
		tech.playSound("mechBoostChargeSound")
		boostShotsFired = boostShotsFired + 1
      end

	  -- Handle charging animation
	  if self.boostPower == 1 then
	    tech.setAnimationState("boost", "charge1")
	  elseif self.boostPower == 2 then
	    tech.setAnimationState("boost", "charge2")
	  elseif self.boostPower == 3 then
	    tech.setAnimationState("boost", "charge3")
	  elseif self.boostPower == 4 then
	    tech.setAnimationState("boost", "charge4")
	  end	  
	  
	elseif self.boostCharging and not self.holdingJump and not self.boostRecharging then
	  -- Ignite boosters
	  self.boostFireTimer = mechBoostDuration * ( 1 + self.boostPower * mechBoostChargeCoefficient )
	  self.boostCharging = false
	  self.boostFiring = true
	  tech.playSound("mechBoostIgniteSound")
	  self.boostDirection = math.sign(math.cos(aimAngle))
	  
	  self.immobileTimer = 0
	  
	  if self.boostPower == 1 then
	    tech.setParticleEmitterActive("boostParticles1", true)
	  elseif self.boostPower == 2 then
	    tech.setParticleEmitterActive("boostParticles2", true)
	  elseif self.boostPower == 3 then
	    tech.setParticleEmitterActive("boostParticles3", true)
	  elseif self.boostPower == 4 then
	    tech.setParticleEmitterActive("boostParticles4", true)
	  end	  
	  
	elseif self.boostFiring and self.boostFireTimer > 0 and not self.boostRecharging then
	  -- Fire boosters
	  self.boostFireTimer = self.boostFireTimer - args.dt
	  	  
	  local modifiedBoostSpeed = mechBoostSpeed * ( 1 + self.boostPower * mechBoostChargeCoefficient )
	  local modifiedBoostPower = mechBoostPower * ( 1 + self.boostPower * mechBoostChargeCoefficient )
	  
	  if self.boostPower == 1 then
	    local diagX = math.cos(mechBoostAngle1)
		local diagY = math.sin(mechBoostAngle1)
	    mcontroller.controlApproachVelocity({self.boostDirection * mechBoostSpeed * diagX, mechBoostSpeed * diagY}, modifiedBoostPower, true, true)
		tech.setAnimationState("boost", "fire1")
	  elseif self.boostPower == 2 then
	    local diagX = math.cos(mechBoostAngle2)
		local diagY = math.sin(mechBoostAngle2)
	    mcontroller.controlApproachVelocity({self.boostDirection * mechBoostSpeed * diagX, mechBoostSpeed * diagY}, modifiedBoostPower, true, true)
		tech.setAnimationState("boost", "fire2")
	  elseif self.boostPower == 3 then
	    local diagX = math.cos(mechBoostAngle3)
		local diagY = math.sin(mechBoostAngle3)
	    mcontroller.controlApproachVelocity({self.boostDirection * mechBoostSpeed * diagX, mechBoostSpeed * diagY}, modifiedBoostPower, true, true)
		tech.setAnimationState("boost", "fire3")
	  elseif self.boostPower == 4 then
	    local diagX = math.cos(mechBoostAngle4)
		local diagY = math.sin(mechBoostAngle4)
	    mcontroller.controlApproachVelocity({self.boostDirection * mechBoostSpeed * diagX, mechBoostSpeed * diagY}, modifiedBoostPower, true, true)
		tech.setAnimationState("boost", "fire4")
	  end	  
	  
	elseif self.boostFiring and self.boostFireTimer <= 0 and not self.boostRecharging then
	  -- Disable boosters, begin recharge period
	  self.boostFireTimer = mechBoostRechargeTime * ( 1 + self.boostPower * mechBoostChargeCoefficient )
	  self.boostFiring = false
	  self.boostRecharging = true 
	  tech.setParticleEmitterActive("boostParticles1", false)
	  tech.setParticleEmitterActive("boostParticles2", false)
	  tech.setParticleEmitterActive("boostParticles3", false)
	  tech.setParticleEmitterActive("boostParticles4", false)
	  
	  tech.playSound("mechBoostRechargeSound")
	  
	elseif self.boostRecharging and self.boostFireTimer > 0 then
	  -- Manage recharge timer, also set animation
	  self.boostFireTimer = self.boostFireTimer - args.dt
	  if self.boostPower == 1 then
	    tech.setAnimationState("boost", "charge1")
	  elseif self.boostPower == 2 then
	    tech.setAnimationState("boost", "charge2")
	  elseif self.boostPower == 3 then
	    tech.setAnimationState("boost", "charge3")
	  elseif self.boostPower == 4 then
	    tech.setAnimationState("boost", "charge4")
	  end	  
	  
	elseif self.boostRecharging and self.boostFireTimer <= 0 then
	  -- Reset all and clean up
	  if self.boostPower == 1 then
		tech.setAnimationState("boost", "recharge1")
	  elseif self.boostPower == 2 then
		tech.setAnimationState("boost", "recharge2")
	  elseif self.boostPower == 3 then
		tech.setAnimationState("boost", "recharge3")
	  elseif self.boostPower == 4 then
		tech.setAnimationState("boost", "recharge4")
	  end 
	  
	  self.boostPower = 0
	  self.boostFireTimer = 0
	  self.boostCharging = false
	  self.boostTimer = 0	
	  self.boostRecharging = false
	  
	  tech.playSound("mechBoostReadySound")
	end
	
	-- Primary weapon system (Rail cannon)
	if self.fireTimer <= 0 then
      if args.actions["mechFire"] and status.resource("energy") > energyCostPerPrimaryShot + 10 then
	    if self.mechFlipped then		  
		  local endPoint = vec_add(mcontroller.position(), {tech.anchorPoint("backGunFirePoint")[1] + mechBeamRange * math.cos(gunAngle), tech.anchorPoint("backGunFirePoint")[2] + mechBeamRange * math.sin(gunAngle) })
		  local u = spawnPenetratingBeam(vec_add(mcontroller.position(), tech.anchorPoint("backGunFirePoint")), endPoint, mechBeamPen, mechBeamLongStep, mechBeamLongProjectile, { power = 0 }, mechBeamEndProjectile, { power = 0 })
		  
		  -- spawnBeam(tech.anchorPoint("backGunFirePoint"), gunAngle, mechBeamRange, mechBeamPen, mechBeamStep, mechBeamLongStep, mechBeamProjectile, mechBeamLongProjectile, { power = 0 }, mechBeamEndProjectile, { power = 0 }, true)
		  spawnDamageBeam(vec_add(mcontroller.position(), tech.anchorPoint("backGunFirePoint")), u, mechBeamWidth, mechBeamEndProjectile, mechBeamEndProjectileConfig)
		  tech.setAnimationState("backFiring", "fire")
		  tech.setAnimationState("arms", "gunBackFiring")
		  tech.setAnimationState("armsAlt", "gunBackFiring")
		else
		  local endPoint = vec_add(mcontroller.position(), {tech.anchorPoint("frontGunFirePoint")[1] + mechBeamRange * math.cos(gunAngle), tech.anchorPoint("frontGunFirePoint")[2] + mechBeamRange * math.sin(gunAngle) })
		  local u = spawnPenetratingBeam(vec_add(mcontroller.position(), tech.anchorPoint("frontGunFirePoint")), endPoint, mechBeamPen, mechBeamLongStep, mechBeamLongProjectile, { power = 0 }, mechBeamEndProjectile, { power = 0 })
		  
		  -- spawnBeam(tech.anchorPoint("frontGunFirePoint"), gunAngle, mechBeamRange, mechBeamPen, mechBeamStep, mechBeamLongStep, mechBeamProjectile, mechBeamLongProjectile, { power = 0 }, mechBeamEndProjectile, { power = 0 }, true)
          spawnDamageBeam(vec_add(mcontroller.position(), tech.anchorPoint("frontGunFirePoint")), u, mechBeamWidth, mechBeamEndProjectile, mechBeamEndProjectileConfig)
		  tech.setAnimationState("frontFiring", "fire")
		  tech.setAnimationState("arms", "gunFrontFiring")
		  tech.setAnimationState("armsAlt", "gunFrontFiring")
		end	

		-- Recoil Force
		local recoilForceDirection = math.sign(-math.cos(aimAngle))
		mcontroller.controlApproachXVelocity(mechRecoilSpeed * recoilForceDirection, mechRecoilPower, true)
		
		tech.playSound("mechFireSound")	
		primaryShotsFired = primaryShotsFired + 1		
		self.fireTimer = mechFireCycle
      end
	else
	  self.fireTimer = self.fireTimer - args.dt
	end
	
	-- Alternate fire system (Grenade box)
	if self.altFireTimer <= 0 and self.altFireCount <= 0 then
	  tech.setAnimationState("grenadeBox", "idle")
      if self.holdingAltFire and status.resource("energy") > energyCostPerAltShot then
	    -- Prime box for firing
	    self.altFireIntervalTimer = 0
	    self.altFireCount = 1
		self.altFireTimer = mechAltFireCycle
		tech.setAnimationState("grenadeBox", "open")
	  end
	elseif self.altFireTimer <= 1.0 and self.altFireCount >= 5 then
	  -- Reloading animation, reset weapon system
	  tech.setAnimationState("grenadeBox", "close")
	  self.altFireCount = 0
	end
	-- Advance Timers
	if self.altFireTimer > 0 then
	  self.altFireTimer = self.altFireTimer - args.dt
	end	
	if self.altFireIntervalTimer > 0 then
	  self.altFireIntervalTimer = self.altFireIntervalTimer - args.dt	
	end
    -- Fire grenades	
	if self.altFireIntervalTimer <= 0 and self.altFireCount > 0 and self.altFireCount < 5 then
	  self.altFireIntervalTimer = mechAltFireShotInterval
	  if self.altFireCount % 2 == 0 then
	    world.spawnProjectile(mechAltProjectile, vec_add(mcontroller.position(), tech.anchorPoint("grenadeBoxFirePoint1")), entity.id(), {math.sign(math.cos(aimAngle)) * 0.754, -0.656}, false, mechAltProjectileConfig)
		tech.playSound("mechAltFireSound")	
	  else
	    world.spawnProjectile(mechAltProjectile, vec_add(mcontroller.position(), tech.anchorPoint("grenadeBoxFirePoint2")), entity.id(), {math.sign(math.cos(aimAngle)) * 0.731, -0.682}, false, mechAltProjectileConfig)
		tech.playSound("mechAltFireSound")		
	  end
	  
	  self.altFireCount = self.altFireCount + 1
	  altShotsFired = altShotsFired + 1
	end


    return 0 -- tech.consumeTechEnergy(energyCostPerSecond * args.dt + altShotsFired * energyCostPerAltShot + primaryShotsFired * energyCostPerPrimaryShot + energyCostPerBoost * boostShotsFired)
  end

  return 0
end
